# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log;
#error_log  /var/log/nginx/error.log  notice;
#error_log  /var/log/nginx/error.log  info;

pid        /run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    # mixing sendfile, tcp_nodelay and tcp_nopush seem to 
    # be as logical as a pacifist joining the Navy Seals
    # TCP_NODELAY option allows Nginx to bypass Nagle algorithm
    # tcp_nopush activates the TCP_CORK option
    # TCP_CORK blocks the data until the packet reaches the 
    # MSS, which equals to the MTU minus the IP header size
    # tcp_nopush MUST be activated with sendfile, which is 
    # exactly where things get interesting
    # Combined to sendfile, tcp_nopush ensures that the 
    # packets are full before being sent to the client. 
    # This greatly reduces network overhead and speeds the
    # way files are sent. Then, when it reaches the last, 
    # probably halt, packet, Nginx removes tcp_nopush. 
    # Then, tcp_nodelay forces the socket to send the data,
    # saving up to 0.2 seconds per file.
    # - https://t37.net/nginx-optimization-understanding-sendfile-tcp_nodelay-and-tcp_nopush.html
    sendfile        on;
    tcp_nopush      on;
    tcp_nodelay     on;

    keepalive_timeout  65;

    gzip  on;

    index   index.html;
    gzip_disable "msie6";

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        server_name _;
        return 444;
    }
}
