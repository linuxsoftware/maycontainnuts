#---------------------------------------------------------------------------
# Companies Rest data
#---------------------------------------------------------------------------

class CoRestDataSource
    constructor: ->
        console.log("NZBN_Glue.CoRestDataSource constructor")
        #@server = "http://www.business.govt.nz"
        @server = "http://maycontainnuts/business"
        @appUrl = "#{@server}/companies/app/service/services/entity"
        @slUrl  = "#{@server}/@@searchlite-companydetails/"

    search: (term, filters, offset) ->
        dfd = new $.Deferred()
        console.log("search for #{term}")
        searchParams = "/search?mode=advanced&start=0&limit=30"
        $.ajax
            'dataType':   "json"
            'url':        @appUrl+searchParams+"&q="+term
        .done (data, textStatus, jqXHR) =>
            results = @_parseSearchResults(data)
            dfd.resolve(results)
            return
        return dfd.promise()

    get: (nzbn) ->
        dfd = new $.Deferred()
        console.log("get #{nzbn}")
        searchParams = "/search?mode=advanced&start=0&limit=30"
        $.ajax
            'dataType':   "json"
            'url':        @appUrl+searchParams+"&q="+nzbn
            'success': (data) =>
                if not data
                    dfd.reject("No data")
                    return
                results = @_parseGetResults(data)
                coNum   = results['Company Number']
                #dtlParams = '/'+results['Company Number']+'/extract/details/all'
                #return $.ajax
                #    'dataType':   "json"
                #    'url':        @appUrl+dtlParams
                $.ajax
                    'url':       @slUrl+coNum
                    'success':   (data) =>
                        @_parseDetailedResults(results, data)
                        dfd.resolve(results)
                # $.get @slUrl+coNum, "", (data) =>
                #     @_parseDetailedResults(results, data)
                #     dfd.resolve(results)
                return
        return dfd.promise()

    addWatch: (userID, nzbn) ->
        dfd = new $.Deferred()
        console.log("add watch on #{nzbn}")
        dfd.reject("Not yet implemented")
        dfd.promise()

    removeWatch: (userID, nzbn) ->
        dfd = new $.Deferred()
        console.log("remove watch on #{nzbn}")
        dfd.reject("Not yet implemented")
        dfd.promise()

    hasWatch: (userID, nzbn) ->
        dfd = new $.Deferred()
        console.log("add watch on #{nzbn}")
        dfd.reject("Not yet implemented")
        dfd.promise()

    _parseSearchResults: (data) ->
        console.log(data)
        disp    = data.displayableCount
        count   = data.totalMatches
        start   = data.start
        end     = data.end
        results = []

        for match in data.list
            results.push(@_parseMatch(match))
        return results

    _parseGetResults: (data) ->
        console.log(data)
        result = null

        if data.totalMatches == 1
            result = @_parseMatch(data.list[0])

        return result

    _parseMatch: (match) ->
        result = {}

        result['Legal Entity Name']   = match['name']
        result['Company Number']      = match['identifier']
        result['NZBN']                = match['nzbn']
        status = []
        statNum = parseInt(match['status'])
        if statNum in [50, 55, 56, 57, 60, 61, 63, 64, 65, 66, 70, 71, 72]
            status.push("ACTIVE")
        else if statNum in [62, 80]
            status.push("INACTIVE")
        if statNum == 50
            status.push("REGISTERED")
        if statNum in [55, 56, 57, 63, 64, 65, 66]
            status.push("VOLUNTARY_ADMINISTRATION")
        if statNum == 72
            status.push("IN_STATUTORY_ADMINISTRATION")
        if statNum in [ 56, 57, 61, 63, 64, 66, 70, 71]
            status.push("IN_RECEIVERSHIP")
        if statNum in [60, 61, 63, 64, 65, 66, 71]
            status.push("IN_LIQUIDATION")
        if statNum in [62, 80]
            status.push("STRUCK_OFF")
        result['Business Status'] = status
        result["Business Type"] = switch match['entityType']
            when 'LTD', 'UNLTD', 'COOP'
                'NZ Company'
            when 'ASIC', 'NON_ASIC'
                'Overseas Company'
        result['Start Date'] = match['incorporationDate']
        result['Registered Address'] = match['entityRegisteredOffice']
        return result

    _parseDetailedResults: (results, data) ->
        console.log(data)
        ## Warning: this could result in us executing JS from this page
        ## e.g. via the <img onerror> attribute
        ## we only do this because we trust Searchlite
        obj = $.parseHTML(data)
        directors = []
        summary = $(obj).find(".companySummary")
        directorsLinks = summary.find('[href^="company-search/directorsearch"]')
        results
        for link in directorsLinks
            directors.push(link.innerHTML)
        results['Directors'] = directors
        return results

$ ->
    NZBN_Glue.ds = new CoRestDataSource()
    return
