#---------------------------------------------------------------------------
# NZBN Rest data
#---------------------------------------------------------------------------

class RestDataSource
    constructor: ->
        console.log("NZBN_Glue.RestDataSource constructor")

    search: (term, filters, offset, limit) ->
        dfd = new $.Deferred()
        console.log("search for #{term}")
        dfd.reject("Not yet implemented")
        dfd.promise()

    get: (nzbn) ->
        dfd = new $.Deferred()
        console.log("get #{nzbn}")
        dfd.reject("Not yet implemented")
        dfd.promise()

    addWatch: (userID, nzbn) ->
        dfd = new $.Deferred()
        console.log("add watch on #{nzbn}")
        dfd.reject("Not yet implemented")
        dfd.promise()

    removeWatch: (userID, nzbn) ->
        dfd = new $.Deferred()
        console.log("remove watch on #{nzbn}")
        dfd.reject("Not yet implemented")
        dfd.promise()

    hasWatch: (userID, nzbn) ->
        dfd = new $.Deferred()
        console.log("add watch on #{nzbn}")
        dfd.reject("Not yet implemented")
        dfd.promise()

$ ->
    NZBN_Glue.ds = new RestDataSource()
    return
