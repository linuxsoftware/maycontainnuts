#---------------------------------------------------------------------------
# NZBN Search
#---------------------------------------------------------------------------

clearOutput = ->
    searchOutputDiv = $(".search-results .output")
    searchStatus = $("#search-status")
    searchOutputDiv.empty()
    searchStatus.text("")
    return

addShortResult = (result) ->
    searchOutputDiv = $(".search-results .output")
    resultDiv = $('<div class="result" />')
    searchOutputDiv.append(resultDiv)
    heading = $('<h3 />')
    name = decodeHtmlEntity(result['Legal Entity Name'])
    if result['NZBN']?
        uri = "#details=#{result['NZBN']}"
        link = $("<a href=\"#{uri}\">#{name}</a>")
        heading.append(link)
    else
        heading.text(name)
    resultDiv.append(heading)
    for key in ["NZBN", "Entity Type", "Company Number", "Trading name",
                "Business Industry Classification", "Business Status"]
        value = result[key]
        if not value
            value = "---"
        fieldDiv = $('<div class="field" />')
        resultDiv.append(fieldDiv)
        fieldDiv.append($('<label>').text(key))
        fieldDiv.append($('<span>').text(value))
    return

addResult = (result) ->
    searchOutputDiv = $(".search-results .output")
    resultDiv = $('<div class="result" />')
    searchOutputDiv.append(resultDiv)
    for key, value of result
        if not value
            value = "---"
        fieldDiv = $('<div class="field" />')
        resultDiv.append(fieldDiv)
        fieldDiv.append($('<label>').text(key))
        if Array.isArray(value)
            newValue = []
            for val in value
                newValue.push(decodeHtmlEntity(val))
            value = newValue
        else
            value = decodeHtmlEntity(value)
        fieldDiv.append($('<span>').text(value))
    return

decodeHtmlEntity = (str) ->
    if str and str.replace
        str = str.replace("&amp;", "&")
        str = str.replace("&lt;", "<")
        str = str.replace("&gt;", ">")
    return str

search = (term, industry, biztypes, bizstatus2, offset=0) ->
    if not term
        return
    filters = {}
    if industry
        filters['Business Industry Classification'] = industry
    if biztypes
        filters['Entity Type'] = biztypes
    if bizstatus2
        filters['Business Status'] = bizstatus2
    NZBN_Glue.search(term, filters, 0, offset+ChunkSize)
    .done (searchResults) ->
        if offset == 0
            clearOutput()
        for result in searchResults['results'][offset..offset+ChunkSize]
            addShortResult(result)
        $("#loadMoreResults").remove()
        if searchResults['count'] < searchResults['matches']
           searchOutputDiv = $(".search-results .output")
           img = $('<img id="loadMoreResults" src="/static/img/ajax-loader.gif"/>')
           searchOutputDiv.append(img)
    .fail (msg) ->
        alert(msg)
    return

get = (nzbn) ->
    NZBN_Glue.get(nzbn)
    .done (result) ->
        clearOutput()
        addResult(result)
    .fail (msg) ->
        alert(msg)
    return

onChangeSync = (elem, key) ->
    value = $.bbq.getState(key) or ""
    if elem.val() != value
        elem.val(value)
    return value

onHashChange = (ev) ->
    term       = onChangeSync $("#searchEntityInput"),   'search'
    industry   = onChangeSync $("#industryFilterInput"), 'industry'

    $("#typeFilterCheckboxes input:checkbox").prop("checked", false)
    biztypes   = $.bbq.getState('biztype') or []
    for value in biztypes
        $("#typeFilterCheckboxes input[value=#{value}]").prop("checked", true)
    bizstatus1 = onChangeSync $("#statusFilterInput"),   'bizstatus1'
    activeStatusFilterInput   = $("#activeStatusFilterInput")
    inactiveStatusFilterInput = $("#inactiveStatusFilterInput")
    if bizstatus1 == "ACTIVE"
        activeStatusFilterInput.show()
        inactiveStatusFilterInput.hide()
        bizstatus2 = onChangeSync activeStatusFilterInput, 'bizstatus2'
    else if bizstatus1 == "INACTIVE"
        activeStatusFilterInput.hide()
        inactiveStatusFilterInput.show()
        bizstatus2 = onChangeSync inactiveStatusFilterInput, 'bizstatus2'
    else
        activeStatusFilterInput.hide()
        inactiveStatusFilterInput.hide()

    if term or industry or biztypes or bizstatus2
        searchStatus = $("#search-status")
        if term.length > 3
            searchStatus.text("Searching for #{term}")
            search(term, industry, biztypes, bizstatus2)
        else
            clearOutput()
    else
        nzbn = $.bbq.getState('details') or ""
        if nzbn.length == 13
            get(nzbn)
        else
            clearOutput()
    return false

loadMoreResults = ->
    term       = $("#searchEntityInput").val()
    industry   = $("#industryFilterInput").val()
    biztypes   = $("#typeFilterCheckboxes input:checkbox:checked").map(->
                     return this.value).get()
    bizstatus1 = $("#statusFilterInput").val()
    if bizstatus1 == "ACTIVE"
        bizstatus2 = $("#activeStatusFilterInput").val()
    else if bizstatus1 == "INACTIVE"
        bizstatus2 = $("#inactiveStatusFilterInput").val()

    loadMoreResults.offset += ChunkSize
    search(term, industry, biztypes, bizstatus2, loadMoreResults.offset)
    return
loadMoreResults.offset = 0

pushSearchState = (key, value) ->
    state = $.bbq.getState()
    delete state['details']
    state[key] = value
    $.bbq.pushState(state, 2)
    return

$ ->
    $(window).on 'hashchange', onHashChange
    searchInput = $("#searchEntityInput")
    searchStatus = $("#search-status")
    searchInput.val("")
    searchInput.on "input propertychange paste", $.debounce 100, (ev) ->
        term = $(this).val()
        if term.length > 3
            pushSearchState('search', term)
        return false
    $("#industryFilterInput").change (ev) ->
        pushSearchState('industry', $(this).val())
        return false
    $("#typeFilterCheckboxes input:checkbox").change (ev) ->
        biztypes = $("#typeFilterCheckboxes input:checkbox:checked").map(->
                         return this.value).get()
        pushSearchState('biztype', biztypes)
        return false

    activeStatusFilterInput = $("#activeStatusFilterInput")
    inactiveStatusFilterInput = $("#inactiveStatusFilterInput")
    $("#statusFilterInput").change (ev) ->
        bizstatus1 = $(this).val()
        pushSearchState('bizstatus1', bizstatus1)
        if bizstatus1 == "ACTIVE"
            pushSearchState('bizstatus2', activeStatusFilterInput.val())
        else if bizstatus1 == "INACTIVE"
            pushSearchState('bizstatus2', inactiveStatusFilterInput.val())
        return false
    activeStatusFilterInput.change (ev) ->
        pushSearchState('bizstatus2', $(this).val())
        return false
    inactiveStatusFilterInput.change (ev) ->
        pushSearchState('bizstatus2', $(this).val())
        return false
    $(window).trigger('hashchange')

    $(window).scroll (ev) ->
        if $(window).scrollTop() == $(document).height() - $(window).height()
            loadMoreResults()
    return

ChunkSize = 20
