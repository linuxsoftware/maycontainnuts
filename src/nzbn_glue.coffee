#---------------------------------------------------------------------------
# NZBN Glue namespace and API
#---------------------------------------------------------------------------
# TODO eliminate this file

class NoDataSource
    constructor: ->
        dfd = new $.Deferred((dfd) -> dfd.reject("No datasource available"))
        @noDataErr = dfd.promise()
    search:      -> return @noDataErr
    get:         -> return @noDataErr
    addWatch:    -> return @noDataErr
    removeWatch: -> return @noDataErr
    hasWatch:    -> return @noDataErr

@NZBN_Glue =
    ds: new NoDataSource()

    search: (term, filters={}, offset=0, limit=200) ->
        if term.match(/94\d{11}/) and offset == 0 and limit > 0
            dfd = new $.Deferred() # to convert error to empty results
            @ds.get(term)
            .done (result) -> dfd.resolve([result])
            .fail (result) -> console.log(result); dfd.resolve([])
            return dfd.promise()
        else
            return @ds.search(term, filters, offset, limit)

    get: (nzbn) ->
        return @ds.get(nzbn)

    login: (userID, nzbn) ->
        return @ds.addWatch(userID, nzbn)

    logout: (userID, nzbn) ->
        return @ds.addWatch(userID, nzbn)

    update: (nzbn, trading_names, registered_address, address_for_service,
             industry, principal_place_of_business, phone_number, email,
             website, effective_from_date) ->
        return @ds.get(nzbn)

    hasWatch: (userID, nzbn) ->
        return @ds.addWatch(userID, nzbn)

    addWatch: (userID, nzbn) ->
        return @ds.addWatch(userID, nzbn)

    removeWatch: (userID, nzbn) ->
        return @ds.removeWatch(userID, nzbn)

