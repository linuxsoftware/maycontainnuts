#---------------------------------------------------------------------------
# NZBN Test data
#---------------------------------------------------------------------------

class TestDataSource
    constructor: ->
        console.log("NZBN_Glue.TestDataSource constructor")
        @watches = {}
        @data = TestData

    search: (term, filters) ->
        dfd = new $.Deferred()
        console.log("search for #{term}")
        term = term.toLowerCase()
        termMatches = (field) ->
            field.toLowerCase().indexOf(term) > -1
        results = $.grep @data, (ent) ->
            termOK = (termMatches(ent['Legal Entity Name']) or
                      termMatches(ent['Trading name']))
            filtersOK = true
            for key, match of filters
                if ent[key] != match
                    filtersOK = false
                    break
            return termOK and filtersOK
        dfd.resolve(results)
        dfd.promise()

    get: (nzbn) ->
        dfd = new $.Deferred()
        console.log("get #{nzbn}")
        results = $.grep(@data, (ent) -> ent.NZBN == nzbn)
        if results.length == 1
            dfd.resolve(results[0])
        else if results.length == 0
            dfd.reject("Did not find")
        else
            dfd.reject("Multiple matches")
        dfd.promise()

    addWatch: (userID, nzbn) ->
        dfd = new $.Deferred()
        console.log("add watch on #{nzbn}")
        @watches[nzbn] = true
        dfd.resolve()
        dfd.promise()

    removeWatch: (userID, nzbn) ->
        dfd = new $.Deferred()
        console.log("remove watch on #{nzbn}")
        @watches[nzbn] = false
        dfd.resolve()
        dfd.promise()

    hasWatch: (userID, nzbn) ->
        dfd = new $.Deferred()
        console.log("add watch on #{nzbn}")
        dfd.resolve(@watches[nzbn] ? false)
        dfd.promise()

$ ->
    NZBN_Glue.ds = new TestDataSource()
    return

TestData = [
  {
    "Business Status": "Active",
    "Website": "http://www.rollinsscope.co.nz",
    "Registered Address": [
      "14l Hackett St",
      "Sudworth",
      "Raineston",
      "4281"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "1 Ellison Ave",
      "Haward",
      "Potter City",
      "4815"
    ],
    "Phone Number": "+64 17 8720501",
    "Legal Entity Name": "Rollinsscope Limited",
    "Directors": [
      "Bianca Bryant"
    ],
    "Company Number": "1801402",
    "Principal Place of Business": [
      "7 Jephson Road",
      "Newman",
      "Wash City",
      "4689"
    ],
    "NZBN": "9429126802274",
    "Email": "admin@rollinsscope.co.nz",
    "Start Date": "07/10/2011",
    "Location": "Wash City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.abrahamssolutions.kiwi",
    "Registered Address": [
      "67 Salomon St",
      "",
      "Ashley City",
      "3185"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Christophers",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "67 Salomon St",
      "",
      "Ashley City",
      "3185"
    ],
    "Phone Number": "+64 14 6946576",
    "Legal Entity Name": "Abrahams Solutions Limited",
    "Directors": [
      "Morgan Gorbold",
      "Lorraine Sharman",
      "Isaac Christinsen",
      "Tamara Walmsley",
      "Troy Ryer"
    ],
    "Company Number": "1802249",
    "Principal Place of Business": [
      "67 Salomon St",
      "",
      "Ashley City",
      "3185"
    ],
    "NZBN": "9429126804339",
    "Email": "info@abrahamssolutions.kiwi",
    "Start Date": "29/07/1986",
    "Location": "Ashley City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.smithtechnologies.nz",
    "Registered Address": [
      "92 Hardwick Rd",
      "",
      "Badcocketown",
      "5941"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Smith Technologies",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "92 Hardwick Rd",
      "",
      "Badcocketown",
      "5941"
    ],
    "Phone Number": "+64 10 7007728",
    "Legal Entity Name": "Smith Technologies Limited",
    "Directors": [
      "Paige Jarvis"
    ],
    "Company Number": "1802453",
    "Principal Place of Business": [
      "92 Hardwick Rd",
      "",
      "Badcocketown",
      "5941"
    ],
    "NZBN": "9429126812570",
    "Email": "admin@smithtechnologies.nz",
    "Start Date": "06/04/2012",
    "Location": "Badcocketown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.fuller.com",
    "Registered Address": [
      "53 Ruggles Rd",
      "",
      "Vanceton",
      "4803"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "53 Ruggles Rd",
      "",
      "Vanceton",
      "4803"
    ],
    "Phone Number": "+64 7 6861903",
    "Legal Entity Name": "Fuller Limited",
    "Directors": [
      "Tammy Mathers",
      "Michael Christinson",
      "Sabrina Dukeson"
    ],
    "Company Number": "1803043",
    "Principal Place of Business": [
      "53 Ruggles Rd",
      "",
      "Vanceton",
      "4803"
    ],
    "NZBN": "9429126821756",
    "Email": "sales@fuller.com",
    "Start Date": "10/04/1997",
    "Location": "Vanceton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.pickle.co.nz",
    "Registered Address": [
      "21 Cookson Highway",
      "",
      "Warrentown",
      "8657"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "21 Cookson Highway",
      "",
      "Warrentown",
      "8657"
    ],
    "Phone Number": "+64 22 8394123",
    "Legal Entity Name": "Pickle Limited",
    "Directors": [
      "Catalina Trask",
      "Gerald Horsfall",
      "Victor Williams"
    ],
    "Company Number": "1803971",
    "Principal Place of Business": [
      "9/685d Pitts Ave",
      "",
      "Arnoldtown",
      "1805"
    ],
    "NZBN": "9429126828632",
    "Email": "info@pickle.co.nz",
    "Start Date": "21/04/1992",
    "Location": "Arnoldtown"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.wyndham.co.nz",
    "Registered Address": [
      "86 Patrick Rd",
      "Vance",
      "Ogdenton",
      "4397"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Wyndham",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "86 Patrick Rd",
      "Vance",
      "Ogdenton",
      "4397"
    ],
    "Phone Number": "+64 27 5373598",
    "Legal Entity Name": "Wyndham Limited",
    "Directors": [
      "Gloria Gore",
      "Keith Donaldson",
      "Clarisa Kimball"
    ],
    "Company Number": "1804380",
    "Principal Place of Business": [
      "957 Horsfall Avenue",
      "Anson",
      "Stephens",
      "3988"
    ],
    "NZBN": "9429126828953",
    "Email": "sales@wyndham.co.nz",
    "Start Date": "25/12/1998",
    "Location": "Stephens"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.megaturnerlands.nz",
    "Registered Address": [
      "37/11 Cartwright Rd",
      "Hampson",
      "Carlisle City",
      "6953"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "37/11 Cartwright Rd",
      "Hampson",
      "Carlisle City",
      "6953"
    ],
    "Phone Number": "+64 21 7125339",
    "Legal Entity Name": "Megaturner Lands Limited",
    "Directors": [
      "Philip Curtis",
      "Jackie Black",
      "Elly Odell"
    ],
    "Company Number": "1805319",
    "Principal Place of Business": [
      "32/43 Brasher Rd",
      "",
      "Edwardsville",
      "2570"
    ],
    "NZBN": "9429126832189",
    "Email": "victor@megaturnerlands.nz",
    "Start Date": "13/05/1988",
    "Location": "Edwardsville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.snider.co.nz",
    "Registered Address": [
      "90 Williams Rd",
      "Christinson",
      "Morris City",
      "3253"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Film and television",
    "Address for Service": [
      "77 Nielson Road",
      "Symons",
      "Winchester City",
      "1767"
    ],
    "Phone Number": "+64 18 6941682",
    "Legal Entity Name": "Snider Limited",
    "Directors": [
      "Sheela Peter",
      "Riley Jackson"
    ],
    "Company Number": "1805602",
    "Principal Place of Business": [
      "33 Sniders Road",
      "Lawson",
      "Bennetville",
      "3217"
    ],
    "NZBN": "9429126834596",
    "Email": "office@snider.co.nz",
    "Start Date": "14/11/1987",
    "Location": "Bennetville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.andersonandjohns.com",
    "Registered Address": [
      "2 William Parade",
      "",
      "Marsdentown",
      "4594"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "2 William Parade",
      "",
      "Marsdentown",
      "4594"
    ],
    "Phone Number": "+64 20 8094717",
    "Legal Entity Name": "Anderson and Johns Limited",
    "Directors": [
      "Hugh Sexton",
      "Nathan Teel",
      "Frederick Clawson",
      "Merritt Rowe",
      "Christa Low",
      "Janice Jeffery",
      "Wendie Winston"
    ],
    "Company Number": "1805778",
    "Principal Place of Business": [
      "23 Moses St",
      "",
      "Roach City",
      "6428"
    ],
    "NZBN": "9429126838297",
    "Email": "sales@andersonandjohns.com",
    "Start Date": "02/07/1995",
    "Location": "Roach City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.dwerryhousetech.nz",
    "Registered Address": [
      "82 Clemens Road",
      "Cookson",
      "Hamptonville",
      "8550"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "28 Potter Avenue",
      "",
      "Christopherville",
      "7605"
    ],
    "Phone Number": "+64 27 7082122",
    "Legal Entity Name": "Dwerryhousetech Limited",
    "Directors": [
      "Jacob Knaggs",
      "Hans Appleby"
    ],
    "Company Number": "1806257",
    "Principal Place of Business": [
      "82 Clemens Road",
      "Cookson",
      "Hamptonville",
      "8550"
    ],
    "NZBN": "9429126845431",
    "Email": "admin@dwerryhousetech.nz",
    "Start Date": "02/03/1998",
    "Location": "Hamptonville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.opss.net.nz",
    "Registered Address": [
      "43 Roscoe Ave",
      "Randall",
      "Eldertown",
      "7621"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "WIWL",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "18 Winfield Ave",
      "",
      "Horneton",
      "2725"
    ],
    "Phone Number": "+64 12 7706110",
    "Legal Entity Name": "OPSS Limited",
    "Directors": [
      "Morgan Causey",
      "Quinn Groves",
      "Dominique Vaughan",
      "Sandi Dick"
    ],
    "Company Number": "1806369",
    "Principal Place of Business": [
      "51 Quigg St",
      "Arrington",
      "Winterbottomton",
      "7957"
    ],
    "NZBN": "9429126851029",
    "Email": "info@opss.net.nz",
    "Start Date": "23/05/2006",
    "Location": "Winterbottomton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.watkinslands.net.nz",
    "Registered Address": [
      "46 Frost Road",
      "Croft",
      "Fairchild",
      "1635"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Watkins Lands",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "42 Jarvis Rd",
      "",
      "Tysonland",
      "6496"
    ],
    "Phone Number": "+64 24 7748609",
    "Legal Entity Name": "Watkins Lands Limited",
    "Directors": [
      "Anthony Longstaff",
      "Tyler Thacker",
      "Beth Pressley",
      "Tucker Bradford",
      "Althea Gabrielson",
      "Steven Elmerson"
    ],
    "Company Number": "1806642",
    "Principal Place of Business": [
      "42 Jarvis Rd",
      "",
      "Tysonland",
      "6496"
    ],
    "NZBN": "9429126855201",
    "Email": "info@watkinslands.net.nz",
    "Start Date": "28/06/1985",
    "Location": "Tysonland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.burrell.nz",
    "Registered Address": [
      "30x Rupertson Highway",
      "Suggitt",
      "Newport",
      "6748"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Burrell",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "60 Ely Parade",
      "Beasley",
      "Snydersland",
      "2185"
    ],
    "Phone Number": "+64 28 6769838",
    "Legal Entity Name": "Burrell Limited",
    "Directors": [
      "Vernon Andrews",
      "Kayley Knight",
      "Clarabelle Peak",
      "Owen Whinery",
      "Alesa Harford",
      "Camelia Booner"
    ],
    "Company Number": "1807014",
    "Principal Place of Business": [
      "30x Rupertson Highway",
      "Suggitt",
      "Newport",
      "6748"
    ],
    "NZBN": "9429126860533",
    "Email": "admin@burrell.nz",
    "Start Date": "19/07/2008",
    "Location": "Newport"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.cliffordandcarpenter.nz",
    "Registered Address": [
      "51 Carter Ave",
      "Atkins",
      "Garrard",
      "1555"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Clifford and Carpenter",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "17/33 Milford Highway",
      "",
      "Stephenstown",
      "6127"
    ],
    "Phone Number": "+64 13 6569087",
    "Legal Entity Name": "Clifford and Carpenter Limited",
    "Directors": [
      "Stella Burnham",
      "Richard Osbourne",
      "Tabitha Riley",
      "Silvia Jeffries",
      "Shelly Pemberton",
      "Cody Longstaff",
      "Daniel Andrews"
    ],
    "Company Number": "1807558",
    "Principal Place of Business": [
      "33f Brewer Rd",
      "",
      "Stephenson City",
      "8169"
    ],
    "NZBN": "9429126864524",
    "Email": "sales@cliffordandcarpenter.nz",
    "Start Date": "23/07/1994",
    "Location": "Stephenson City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.adac.nz",
    "Registered Address": [
      "99 Rogers Ave",
      "Ayton",
      "Osbornetown",
      "6900"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "99 Rogers Ave",
      "Ayton",
      "Osbornetown",
      "6900"
    ],
    "Phone Number": "+64 25 8409712",
    "Legal Entity Name": "ADAC Limited",
    "Directors": [
      "Zebulon Hawk",
      "Florens Fishman",
      "Eve Jernigan"
    ],
    "Company Number": "1808466",
    "Principal Place of Business": [
      "38 Hudnall Road",
      "",
      "Tobiasland",
      "1823"
    ],
    "NZBN": "9429126866368",
    "Email": "admin@adac.nz",
    "Start Date": "10/02/1989",
    "Location": "Tobiasland"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.josephs.co.nz",
    "Registered Address": [
      "86 Samuelson Ave",
      "Quick",
      "Pope",
      "2575"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Intersimons",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "46/1 Blake Rd",
      "",
      "Fullerton",
      "5087"
    ],
    "Phone Number": "+64 12 6860588",
    "Legal Entity Name": "Josephs Limited",
    "Directors": [
      "Goloria Christians",
      "Foster Hewitt"
    ],
    "Company Number": "1808823",
    "Principal Place of Business": [
      "12/59o Pelley Highway",
      "Whittle",
      "Woodwardtown",
      "3319"
    ],
    "NZBN": "9429126871249",
    "Email": "daniel@josephs.co.nz",
    "Start Date": "14/06/1993",
    "Location": "Woodwardtown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.kimball.nz",
    "Registered Address": [
      "21/37h Jeffers Highway",
      "",
      "Quickley City",
      "1283"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Kimball",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "38/37 Cox Road",
      "Pelley",
      "Keysville",
      "3396"
    ],
    "Phone Number": "+64 8 7633083",
    "Legal Entity Name": "Kimball Limited",
    "Directors": [
      "Bryson Hutson",
      "Vaughn Elmerson",
      "Patrick Morriss",
      "Steph Garner",
      "Liana Dwerryhouse",
      "Shiela Bass",
      "Millie Cross",
      "Ryan Crawford"
    ],
    "Company Number": "1809377",
    "Principal Place of Business": [
      "21/37h Jeffers Highway",
      "",
      "Quickley City",
      "1283"
    ],
    "NZBN": "9429126871942",
    "Email": "info@kimball.nz",
    "Start Date": "04/04/2009",
    "Location": "Quickley City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.emersongarrardandshelby.co.nz",
    "Registered Address": [
      "70 Rowntree Ave",
      "Witherspoon",
      "Morrisontown",
      "4356"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Mondy, Wilkins and Seaver",
    "Business Industry Classification": "Film and television",
    "Address for Service": [
      "48/38 Tipton Highway",
      "Hull",
      "Mosesland",
      "5451"
    ],
    "Phone Number": "+64 5 8979403",
    "Legal Entity Name": "Emerson, Garrard and Shelby Limited",
    "Directors": [
      "Sadie Spencer",
      "Courtney Carter"
    ],
    "Company Number": "1809990",
    "Principal Place of Business": [
      "48/38 Tipton Highway",
      "Hull",
      "Mosesland",
      "5451"
    ],
    "NZBN": "9429126880760",
    "Email": "lydia@emersongarrardandshelby.co.nz",
    "Start Date": "10/06/2014",
    "Location": "Mosesland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.chancechristinsenandking.net",
    "Registered Address": [
      "82q Norwood Rd",
      "Byrd",
      "Ryleytown",
      "5628"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "73 Duke Ave",
      "Ryeley",
      "Hewitt City",
      "7306"
    ],
    "Phone Number": "+64 16 5638824",
    "Legal Entity Name": "Chance, Christinsen and King Limited",
    "Directors": [
      "Gerald Wash",
      "Vincent Arterberry",
      "Jody Shakesheave",
      "Sharlotte Tuff"
    ],
    "Company Number": "1810660",
    "Principal Place of Business": [
      "43/11u Acker Highway",
      "Sargent",
      "Cristiansonland",
      "2214"
    ],
    "NZBN": "9429126888223",
    "Email": "admin@chancechristinsenandking.net",
    "Start Date": "05/09/2015",
    "Location": "Cristiansonland"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.megaovertonnetworks.nz",
    "Registered Address": [
      "42/2 Bradford St",
      "",
      "Herbertsonland",
      "4312"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "38 Howland St",
      "Howard",
      "Josephsonton",
      "6296"
    ],
    "Phone Number": "+64 19 7207577",
    "Legal Entity Name": "Megaoverton Networks Limited",
    "Directors": [
      "Christian Midgley"
    ],
    "Company Number": "1811080",
    "Principal Place of Business": [
      "34 Ethanson Road",
      "",
      "Hobsonland",
      "5646"
    ],
    "NZBN": "9429126895573",
    "Email": "admin@megaovertonnetworks.nz",
    "Start Date": "09/04/1996",
    "Location": "Hobsonland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.frd.co.nz",
    "Registered Address": [
      "44 Hume St",
      "Pitts",
      "Gibb",
      "2758"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "FRD",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "79 Kersey St",
      "Quigley",
      "Cross City",
      "3718"
    ],
    "Phone Number": "+64 23 8539375",
    "Legal Entity Name": "FRD Limited",
    "Directors": [
      "Ignatius Payne",
      "Perry Goode",
      "Carlene Mercer",
      "Arabella Bonney"
    ],
    "Company Number": "1811386",
    "Principal Place of Business": [
      "44 Hume St",
      "Pitts",
      "Gibb",
      "2758"
    ],
    "NZBN": "9429126902769",
    "Email": "brandon@frd.co.nz",
    "Start Date": "04/11/1986",
    "Location": "Gibb"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.gnr.com",
    "Registered Address": [
      "101k Willis Rd",
      "Emerson",
      "Blackmanville",
      "2198"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "IXI",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "101k Willis Rd",
      "Emerson",
      "Blackmanville",
      "2198"
    ],
    "Phone Number": "+64 21 6870818",
    "Legal Entity Name": "GNR Limited",
    "Directors": [
      "Sally Bancroft",
      "Wesley Pitts",
      "Hans Tobias",
      "Roxana Page",
      "Elma Wilkins",
      "Lindsay Thacker",
      "Victor Tyson",
      "Elisa Kirby"
    ],
    "Company Number": "1811823",
    "Principal Place of Business": [
      "101k Willis Rd",
      "Emerson",
      "Blackmanville",
      "2198"
    ],
    "NZBN": "9429126906019",
    "Email": "sales@gnr.com",
    "Start Date": "08/06/2003",
    "Location": "Blackmanville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.clintonandforester.net.nz",
    "Registered Address": [
      "21b Masters Rd",
      "Butts",
      "Allsopptown",
      "3599"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "35 Richardson Ave",
      "Paternoster",
      "Stevensland",
      "7184"
    ],
    "Phone Number": "+64 28 7653793",
    "Legal Entity Name": "Clinton and Forester Limited",
    "Directors": [
      "Prudence Alfredson",
      "Jeana Underwood",
      "Charles Bloxam",
      "Norman Banister",
      "Veronica Martinson",
      "Glenn Revie",
      "Jan Foster"
    ],
    "Company Number": "1812535",
    "Principal Place of Business": [
      "35 Richardson Ave",
      "Paternoster",
      "Stevensland",
      "7184"
    ],
    "NZBN": "9429126912607",
    "Email": "kevin@clintonandforester.net.nz",
    "Start Date": "22/07/2000",
    "Location": "Stevensland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.interhuxleyscape.co.nz",
    "Registered Address": [
      "34 Tate Road",
      "",
      "Baldwinton",
      "2786"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Interhuxleyscape",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "817 Carter Avenue",
      "",
      "Huddlesonton",
      "5746"
    ],
    "Phone Number": "+64 11 8602539",
    "Legal Entity Name": "Interhuxleyscape Limited",
    "Directors": [
      "Henry Eason",
      "Henry Sowards",
      "Aideen Porcher",
      "Florens Neville",
      "Camillah Queshire",
      "Rayna Tate",
      "Gabrielle Rier"
    ],
    "Company Number": "1813191",
    "Principal Place of Business": [
      "54a Sargent Rd",
      "Danell",
      "Quincey City",
      "7164"
    ],
    "NZBN": "9429126914960",
    "Email": "kali@interhuxleyscape.co.nz",
    "Start Date": "28/01/1995",
    "Location": "Quincey City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.aytonandbadcock.com",
    "Registered Address": [
      "5y Rakes Highway",
      "Merrick",
      "Newportland",
      "2530"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Michaels Solutions",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "968n Tinker Street",
      "Tupper",
      "Warrenville",
      "6098"
    ],
    "Phone Number": "+64 19 5586349",
    "Legal Entity Name": "Ayton and Badcock Limited",
    "Directors": [
      "Flynn Loman",
      "Dannah Mondy"
    ],
    "Company Number": "1813850",
    "Principal Place of Business": [
      "968n Tinker Street",
      "Tupper",
      "Warrenville",
      "6098"
    ],
    "NZBN": "9429126921135",
    "Email": "sales@aytonandbadcock.com",
    "Start Date": "09/04/1990",
    "Location": "Warrenville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.jeromesoftware.nz",
    "Registered Address": [
      "75 Constable Road",
      "Mondy",
      "Westbrookton",
      "7790"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "79 Jerome Ave",
      "Samuels",
      "Christinsonland",
      "1540"
    ],
    "Phone Number": "+64 26 6870442",
    "Legal Entity Name": "Jeromesoftware Limited",
    "Directors": [
      "Yancy Coburn",
      "Alcinda Cantrell"
    ],
    "Company Number": "1814440",
    "Principal Place of Business": [
      "2/89x Virgo St",
      "",
      "Earltown",
      "8479"
    ],
    "NZBN": "9429126924716",
    "Email": "sales@jeromesoftware.nz",
    "Start Date": "11/07/2003",
    "Location": "Earltown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.michaelssolutions.net",
    "Registered Address": [
      "21 Little Ave",
      "Young",
      "Howardtown",
      "4238"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Michaels Solutions",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "19 Wallace St",
      "Jeffery",
      "Sackvilleton",
      "1568"
    ],
    "Phone Number": "+64 24 8620281",
    "Legal Entity Name": "Michaels Solutions Limited",
    "Directors": [
      "Monica Longstaff",
      "Selena Reier"
    ],
    "Company Number": "1815173",
    "Principal Place of Business": [
      "19 Wallace St",
      "Jeffery",
      "Sackvilleton",
      "1568"
    ],
    "NZBN": "9429126926871",
    "Email": "admin@michaelssolutions.net",
    "Start Date": "24/10/2014",
    "Location": "Sackvilleton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ril.net.nz",
    "Registered Address": [
      "5 Street Street",
      "",
      "Bentonville",
      "7922"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "RIL",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "6 Midgley St",
      "Travers",
      "Dexter",
      "3645"
    ],
    "Phone Number": "+64 21 7606150",
    "Legal Entity Name": "RIL Limited",
    "Directors": [
      "Melodie Causer",
      "John Rake",
      "Zane Ash",
      "Vincent Hargrave"
    ],
    "Company Number": "1815968",
    "Principal Place of Business": [
      "5 Street Street",
      "",
      "Bentonville",
      "7922"
    ],
    "NZBN": "9429126935781",
    "Email": "office@ril.net.nz",
    "Start Date": "11/10/1991",
    "Location": "Bentonville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.baileynathansonandrowntree.com",
    "Registered Address": [
      "20 Henryson Rd",
      "Eldridge",
      "Hathewayland",
      "5920"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "6v Sutton Street",
      "Autenberry",
      "Tolbert",
      "1678"
    ],
    "Phone Number": "+64 28 5875759",
    "Legal Entity Name": "Bailey, Nathanson and Rowntree Limited",
    "Directors": [
      "Abigail Harris",
      "Julie Mason"
    ],
    "Company Number": "1816863",
    "Principal Place of Business": [
      "20 Henryson Rd",
      "Eldridge",
      "Hathewayland",
      "5920"
    ],
    "NZBN": "9429126942017",
    "Email": "aaron@baileynathansonandrowntree.com",
    "Start Date": "14/03/1986",
    "Location": "Hathewayland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.bristolandwortham.nz",
    "Registered Address": [
      "62 Skinner Rd",
      "Gibbs",
      "Northland",
      "4895"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "62 Skinner Rd",
      "Gibbs",
      "Northland",
      "4895"
    ],
    "Phone Number": "+64 12 8421000",
    "Legal Entity Name": "Bristol and Wortham Limited",
    "Directors": [
      "Eric Fairbairn",
      "Tyler Fuller",
      "Maria Byrd"
    ],
    "Company Number": "1817842",
    "Principal Place of Business": [
      "62 Skinner Rd",
      "Gibbs",
      "Northland",
      "4895"
    ],
    "NZBN": "9429126946084",
    "Email": "admin@bristolandwortham.nz",
    "Start Date": "01/11/1999",
    "Location": "Northland"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.rugglesandyoung.kiwi",
    "Registered Address": [
      "37/11 Wilcox Street",
      "Bullock",
      "Beckhamton",
      "6461"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Ruggles and Young",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "37/11 Wilcox Street",
      "Bullock",
      "Beckhamton",
      "6461"
    ],
    "Phone Number": "+64 29 6191863",
    "Legal Entity Name": "Ruggles and Young Limited",
    "Directors": [
      "Alexander Barnes",
      "Gwyneth. Wakefield",
      "Heather Fabian",
      "Kenneth Clinton",
      "Betsy Peters",
      "Hugh Garrard",
      "Virgil Montgomery"
    ],
    "Company Number": "1817941",
    "Principal Place of Business": [
      "37/11 Wilcox Street",
      "Bullock",
      "Beckhamton",
      "6461"
    ],
    "NZBN": "9429126946695",
    "Email": "admin@rugglesandyoung.kiwi",
    "Start Date": "30/03/1989",
    "Location": "Beckhamton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.vipondandalvey.net",
    "Registered Address": [
      "47u Mathews Rd",
      "Tipton",
      "Popeton",
      "6706"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "95 Foster Rd",
      "Deadman",
      "Christopher City",
      "8005"
    ],
    "Phone Number": "+64 21 8673812",
    "Legal Entity Name": "Vipond and Alvey Limited",
    "Directors": [
      "Keith Disney",
      "Elisa Street",
      "William Bishop",
      "Eden Ayton",
      "Alexa Beckett",
      "Nicholas Atterberry"
    ],
    "Company Number": "1818570",
    "Principal Place of Business": [
      "47u Mathews Rd",
      "Tipton",
      "Popeton",
      "6706"
    ],
    "NZBN": "9429126951170",
    "Email": "sally@vipondandalvey.net",
    "Start Date": "29/04/1998",
    "Location": "Popeton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.kerseyandphilips.nz",
    "Registered Address": [
      "47 Honeysett Avenue",
      "Hardwick",
      "Outterridgetown",
      "3769"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Kersey and Philips",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "38/31 Tittensor St",
      "Bristow",
      "Carpenterton",
      "3664"
    ],
    "Phone Number": "+64 28 8698331",
    "Legal Entity Name": "Kersey and Philips Limited",
    "Directors": [
      "Addie Reier"
    ],
    "Company Number": "1818827",
    "Principal Place of Business": [
      "47 Honeysett Avenue",
      "Hardwick",
      "Outterridgetown",
      "3769"
    ],
    "NZBN": "9429126956410",
    "Email": "owen@kerseyandphilips.nz",
    "Start Date": "25/09/2000",
    "Location": "Outterridgetown"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.klk.co.nz",
    "Registered Address": [
      "33 Horsfall Rd",
      "Paternoster",
      "Shelby",
      "4872"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "33 Horsfall Rd",
      "Paternoster",
      "Shelby",
      "4872"
    ],
    "Phone Number": "+64 10 7415472",
    "Legal Entity Name": "KLK Limited",
    "Directors": [
      "Parker Dalton",
      "Joanna Southers",
      "Robert Jeffers"
    ],
    "Company Number": "1818953",
    "Principal Place of Business": [
      "59 Cantrell Rd",
      "Hollands",
      "Barlowton",
      "4761"
    ],
    "NZBN": "9429126965207",
    "Email": "helaine@klk.co.nz",
    "Start Date": "14/05/2003",
    "Location": "Barlowton"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.nkn.nz",
    "Registered Address": [
      "52 Witherspoon Road",
      "",
      "Pooletown",
      "5036"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "76 Miles Avenue",
      "Kipling",
      "Horsfall",
      "3515"
    ],
    "Phone Number": "+64 15 5275470",
    "Legal Entity Name": "NKN Limited",
    "Directors": [
      "Gavin Kidd",
      "Katie Harden",
      "John Huff"
    ],
    "Company Number": "1819910",
    "Principal Place of Business": [
      "36/35 Myers Ave",
      "Rowbottom",
      "Gabrielston",
      "8162"
    ],
    "NZBN": "9429126972595",
    "Email": "info@nkn.nz",
    "Start Date": "20/01/1995",
    "Location": "Gabrielston"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.fullerandhowland.net.nz",
    "Registered Address": [
      "41s Savidge Street",
      "Simon",
      "Polleytown",
      "5481"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "41s Savidge Street",
      "Simon",
      "Polleytown",
      "5481"
    ],
    "Phone Number": "+64 20 7519271",
    "Legal Entity Name": "Fuller and Howland Limited",
    "Directors": [
      "Isaac Hawk",
      "Harold Donaldson",
      "Adam Platt",
      "Janie Anthonyson",
      "Kurt Beasley",
      "Trevor Gore"
    ],
    "Company Number": "1820390",
    "Principal Place of Business": [
      "41s Savidge Street",
      "Simon",
      "Polleytown",
      "5481"
    ],
    "NZBN": "9429126977378",
    "Email": "tessi@fullerandhowland.net.nz",
    "Start Date": "21/07/2010",
    "Location": "Polleytown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.petitlands.kiwi",
    "Registered Address": [
      "81 Jefferson Road",
      "",
      "Bateson",
      "1719"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Petit Lands",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "19/20 Kitchens Street",
      "Triggs",
      "Elmerson City",
      "6956"
    ],
    "Phone Number": "+64 25 7354681",
    "Legal Entity Name": "Petit Lands Limited",
    "Directors": [
      "Nicola Badcock",
      "Jo Bloodworth",
      "Lawrence Hull"
    ],
    "Company Number": "1821337",
    "Principal Place of Business": [
      "46/561 Ellison Ave",
      "Simmons",
      "Simsville",
      "3906"
    ],
    "NZBN": "9429126982259",
    "Email": "sales@petitlands.kiwi",
    "Start Date": "14/08/1988",
    "Location": "Simsville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.earlson.com",
    "Registered Address": [
      "83 Hooker Rd",
      "Spears",
      "Herbertsland",
      "4252"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "43 Steed Avenue",
      "Salvage",
      "Alfsontown",
      "7536"
    ],
    "Phone Number": "+64 27 8245628",
    "Legal Entity Name": "Earlson Limited",
    "Directors": [
      "Brady King",
      "Cristina Kendrick",
      "Louis Snyder",
      "Eli Cotterill",
      "Wilson Curtis"
    ],
    "Company Number": "1821937",
    "Principal Place of Business": [
      "43 Steed Avenue",
      "Salvage",
      "Alfsontown",
      "7536"
    ],
    "NZBN": "9429126988169",
    "Email": "office@earlson.com",
    "Start Date": "18/10/1985",
    "Location": "Alfsontown"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.rice.com",
    "Registered Address": [
      "22g Salomon Avenue",
      "Kitchens",
      "Gabrielson",
      "1207"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "8o Hendry St",
      "",
      "Denmanton",
      "2041"
    ],
    "Phone Number": "+64 14 5697276",
    "Legal Entity Name": "Rice Limited",
    "Directors": [
      "Ryan Spence"
    ],
    "Company Number": "1822140",
    "Principal Place of Business": [
      "8o Hendry St",
      "",
      "Denmanton",
      "2041"
    ],
    "NZBN": "9429126993484",
    "Email": "gordon@rice.com",
    "Start Date": "27/06/2008",
    "Location": "Denmanton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.atkinstechnologies.com",
    "Registered Address": [
      "49 Pryor St",
      "",
      "Blueville",
      "3186"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Danniel Networks",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "37 Dennell St",
      "",
      "Lawson City",
      "3753"
    ],
    "Phone Number": "+64 9 8446423",
    "Legal Entity Name": "Atkins Technologies Limited",
    "Directors": [
      "Sander Marley",
      "Ethan Kersey",
      "Andrew Pemberton",
      "Trevor Bond"
    ],
    "Company Number": "1822634",
    "Principal Place of Business": [
      "72 Gibbs Ave",
      "Dexter",
      "Hogarthville",
      "4612"
    ],
    "NZBN": "9429127002239",
    "Email": "admin@atkinstechnologies.com",
    "Start Date": "02/01/1985",
    "Location": "Hogarthville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.owstonbancroftandavery.com",
    "Registered Address": [
      "3 Masterson Road",
      "Easom",
      "Bernardton",
      "6862"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Owston, Bancroft and Avery",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "49 Vernon Parade",
      "Hart",
      "Kendallland",
      "2353"
    ],
    "Phone Number": "+64 10 7327523",
    "Legal Entity Name": "Owston, Bancroft and Avery Limited",
    "Directors": [
      "Delyth Tailor"
    ],
    "Company Number": "1823571",
    "Principal Place of Business": [
      "49 Vernon Parade",
      "Hart",
      "Kendallland",
      "2353"
    ],
    "NZBN": "9429127009825",
    "Email": "info@owstonbancroftandavery.com",
    "Start Date": "13/12/1995",
    "Location": "Kendallland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.nyecorp.nz",
    "Registered Address": [
      "68 Stern Road",
      "Rennold",
      "Mathers",
      "4799"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "ALN",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "68 Stern Road",
      "Rennold",
      "Mathers",
      "4799"
    ],
    "Phone Number": "+64 8 5927261",
    "Legal Entity Name": "Nyecorp Limited",
    "Directors": [
      "Geraldine Ruggles",
      "Janita Atkinson",
      "Tessi Prescott",
      "Edward Merritt",
      "Christopher Blakeslee",
      "Glenn Triggs"
    ],
    "Company Number": "1824487",
    "Principal Place of Business": [
      "10/72 Hodges Street",
      "Haight",
      "Clemensville",
      "5589"
    ],
    "NZBN": "9429127017820",
    "Email": "sales@nyecorp.nz",
    "Start Date": "09/10/2012",
    "Location": "Clemensville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.reviemilesanddaniel.nz",
    "Registered Address": [
      "39/76 Upton Street",
      "Honeycutt",
      "Jephsonville",
      "2271"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Badcock and Causey",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "7/49 Aiken Ave",
      "Clayton",
      "Gileston",
      "6495"
    ],
    "Phone Number": "+64 6 7815661",
    "Legal Entity Name": "Revie, Miles and Daniel Limited",
    "Directors": [
      "Newman Daniel",
      "Orson Stidolph",
      "Justin Milford",
      "John Arthur",
      "Beverly Gibson",
      "Vaughn Matthewson"
    ],
    "Company Number": "1825026",
    "Principal Place of Business": [
      "39/76 Upton Street",
      "Honeycutt",
      "Jephsonville",
      "2271"
    ],
    "NZBN": "9429127022657",
    "Email": "info@reviemilesanddaniel.nz",
    "Start Date": "05/08/1986",
    "Location": "Jephsonville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.virgoscope.co.nz",
    "Registered Address": [
      "6 Jones Ave",
      "Winthrop",
      "Flemington",
      "7505"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "6 Jones Ave",
      "Winthrop",
      "Flemington",
      "7505"
    ],
    "Phone Number": "+64 12 8960014",
    "Legal Entity Name": "Virgoscope Limited",
    "Directors": [
      "Sander Ibbott"
    ],
    "Company Number": "1825619",
    "Principal Place of Business": [
      "6 Jones Ave",
      "Winthrop",
      "Flemington",
      "7505"
    ],
    "NZBN": "9429127028550",
    "Email": "info@virgoscope.co.nz",
    "Start Date": "22/02/1991",
    "Location": "Flemington"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.cumminsscape.kiwi",
    "Registered Address": [
      "11 Wheeler St",
      "Quick",
      "Cox City",
      "7338"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Cumminsscape",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "12 Collingwood Highway",
      "",
      "Linwoodton",
      "7424"
    ],
    "Phone Number": "+64 11 8433317",
    "Legal Entity Name": "Cumminsscape Limited",
    "Directors": [
      "Tyler Wyatt"
    ],
    "Company Number": "1825913",
    "Principal Place of Business": [
      "11 Wheeler St",
      "Quick",
      "Cox City",
      "7338"
    ],
    "NZBN": "9429127035695",
    "Email": "sales@cumminsscape.kiwi",
    "Start Date": "24/12/2011",
    "Location": "Cox City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.jamisonexperience.nz",
    "Registered Address": [
      "95 Eccleston Ave",
      "",
      "Watsonville",
      "4048"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "William and Appleton",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "95 Eccleston Ave",
      "",
      "Watsonville",
      "4048"
    ],
    "Phone Number": "+64 29 6247648",
    "Legal Entity Name": "Jamison Experience Limited",
    "Directors": [
      "Garrett Bryson"
    ],
    "Company Number": "1826537",
    "Principal Place of Business": [
      "67 Augustine Ave",
      "",
      "Huddlestonland",
      "6373"
    ],
    "NZBN": "9429127040255",
    "Email": "merritt@jamisonexperience.nz",
    "Start Date": "13/08/2015",
    "Location": "Huddlestonland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.dennellandjosephson.co.nz",
    "Registered Address": [
      "48/75 Denman Ave",
      "Mark",
      "Lowland",
      "2646"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "84u Bloxam Highway",
      "Scrivener",
      "Ryer City",
      "2287"
    ],
    "Phone Number": "+64 25 8358055",
    "Legal Entity Name": "Dennell and Josephson Limited",
    "Directors": [
      "Tucker Savidge",
      "Kelsey Shelby",
      "Nigel Howse",
      "Emily Law",
      "Edward Pettigrew"
    ],
    "Company Number": "1826923",
    "Principal Place of Business": [
      "42 Kirby Rd",
      "",
      "Auteberrytown",
      "4210"
    ],
    "NZBN": "9429127042686",
    "Email": "office@dennellandjosephson.co.nz",
    "Start Date": "06/03/2005",
    "Location": "Auteberrytown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.anthonysonandwoodham.co.nz",
    "Registered Address": [
      "17 Cotterill St",
      "",
      "Bishopton",
      "4177"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "27/73 Pitts Rd",
      "",
      "Millhouseton",
      "4487"
    ],
    "Phone Number": "+64 8 6347701",
    "Legal Entity Name": "Anthonyson and Woodham Limited",
    "Directors": [
      "Christopher Granger",
      "Delwyn Banister",
      "Erina Garner",
      "Allison Herberts",
      "Jeremy Morse",
      "Klarysa Fletcher",
      "William Ridley"
    ],
    "Company Number": "1827873",
    "Principal Place of Business": [
      "72a Joiner St",
      "",
      "Weavertown",
      "1745"
    ],
    "NZBN": "9429127044970",
    "Email": "sander@anthonysonandwoodham.co.nz",
    "Start Date": "26/01/1996",
    "Location": "Weavertown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.staffordenterprises.kiwi",
    "Registered Address": [
      "89s Clifford Parade",
      "Clark",
      "Eadston",
      "8580"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Stafford Enterprises",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "89s Clifford Parade",
      "Clark",
      "Eadston",
      "8580"
    ],
    "Phone Number": "+64 17 7421838",
    "Legal Entity Name": "Stafford Enterprises Limited",
    "Directors": [
      "Nelli Plank"
    ],
    "Company Number": "1828457",
    "Principal Place of Business": [
      "10/85 Reynolds Parade",
      "Murgatroyd",
      "Harrell City",
      "3452"
    ],
    "NZBN": "9429127053835",
    "Email": "admin@staffordenterprises.kiwi",
    "Start Date": "17/06/1989",
    "Location": "Harrell City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.speightandscrivener.kiwi",
    "Registered Address": [
      "8 Crouch Ave",
      "",
      "Trengoveville",
      "6178"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "4 Miller Parade",
      "Alberts",
      "Raynersonland",
      "2667"
    ],
    "Phone Number": "+64 6 7898623",
    "Legal Entity Name": "Speight and Scrivener Limited",
    "Directors": [
      "Brian Holmes",
      "James Wilkinson",
      "Steven Rounds",
      "Vernon Wakefield"
    ],
    "Company Number": "1829147",
    "Principal Place of Business": [
      "64 Spears St",
      "Smith",
      "Way",
      "5126"
    ],
    "NZBN": "9429127061342",
    "Email": "sales@speightandscrivener.kiwi",
    "Start Date": "23/10/2002",
    "Location": "Way"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.benbowcorp.nz",
    "Registered Address": [
      "2u Queen Highway",
      "Rowbottom",
      "Readdie City",
      "7054"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Murgatroydscape",
    "Business Industry Classification": "Film and television",
    "Address for Service": [
      "47 Midgley Highway",
      "Chapman",
      "Paulsonton",
      "7207"
    ],
    "Phone Number": "+64 10 6213621",
    "Legal Entity Name": "Benbowcorp Limited",
    "Directors": [
      "Tyler Kendall",
      "Derek Knight"
    ],
    "Company Number": "1829349",
    "Principal Place of Business": [
      "19/54t Bloodworth St",
      "Booner",
      "Sims",
      "3758"
    ],
    "NZBN": "9429127066514",
    "Email": "sales@benbowcorp.nz",
    "Start Date": "03/06/1998",
    "Location": "Sims"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.mercerhopperandhermanson.nz",
    "Registered Address": [
      "6/33 Firmin Ave",
      "Henson",
      "Woodcock",
      "8707"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "16a Winterbottom Rd",
      "",
      "Garrod City",
      "4118"
    ],
    "Phone Number": "+64 25 6970875",
    "Legal Entity Name": "Mercer, Hopper and Hermanson Limited",
    "Directors": [
      "Ann Sanders",
      "Sasha Stroud"
    ],
    "Company Number": "1830059",
    "Principal Place of Business": [
      "16a Winterbottom Rd",
      "",
      "Garrod City",
      "4118"
    ],
    "NZBN": "9429127074625",
    "Email": "admin@mercerhopperandhermanson.nz",
    "Start Date": "14/07/2007",
    "Location": "Garrod City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.sempersexperience.net",
    "Registered Address": [
      "54 Hanley Rd",
      "Holt",
      "Pryorton",
      "5609"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Sempers Experience",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "36 Bloodworth Avenue",
      "Boivin",
      "Sterntown",
      "5956"
    ],
    "Phone Number": "+64 9 5735605",
    "Legal Entity Name": "Sempers Experience Limited",
    "Directors": [
      "Dean Espenson",
      "Wade Bonham",
      "Zane Michaelson",
      "Kirstin Tuff",
      "Ainsley Simpson"
    ],
    "Company Number": "1831003",
    "Principal Place of Business": [
      "54 Hanley Rd",
      "Holt",
      "Pryorton",
      "5609"
    ],
    "NZBN": "9429127076889",
    "Email": "admin@sempersexperience.net",
    "Start Date": "24/04/2013",
    "Location": "Pryorton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.north.nz",
    "Registered Address": [
      "12 Thorpe Street",
      "",
      "Wood",
      "1243"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "12 Thorpe Street",
      "",
      "Wood",
      "1243"
    ],
    "Phone Number": "+64 9 7275367",
    "Legal Entity Name": "North Limited",
    "Directors": [
      "Xavier Phillips",
      "Troy Draper",
      "Bernice Newman",
      "Fay Andrewson",
      "Amy Varnham",
      "Rhonda Baker"
    ],
    "Company Number": "1831904",
    "Principal Place of Business": [
      "491w Starr Street",
      "Deadman",
      "Burketon",
      "4239"
    ],
    "NZBN": "9429127083450",
    "Email": "admin@north.nz",
    "Start Date": "11/05/2010",
    "Location": "Burketon"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.arthurandwheelock.nz",
    "Registered Address": [
      "91n Brasher Rd",
      "Sims",
      "Trent City",
      "8762"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Arthur and Wheelock",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "91n Brasher Rd",
      "Sims",
      "Trent City",
      "8762"
    ],
    "Phone Number": "+64 17 8595319",
    "Legal Entity Name": "Arthur and Wheelock Limited",
    "Directors": [
      "Jacqueline Warren",
      "Wilfred Trask",
      "Rhianna Rogers",
      "Garrett Fry",
      "Melissa Clawson"
    ],
    "Company Number": "1832334",
    "Principal Place of Business": [
      "91n Brasher Rd",
      "Sims",
      "Trent City",
      "8762"
    ],
    "NZBN": "9429127089575",
    "Email": "admin@arthurandwheelock.nz",
    "Start Date": "04/11/1988",
    "Location": "Trent City"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.vaughantrode.co.nz",
    "Registered Address": [
      "211 Jeffries Rd",
      "",
      "Barlow",
      "6227"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "211 Jeffries Rd",
      "",
      "Barlow",
      "6227"
    ],
    "Phone Number": "+64 27 6468364",
    "Legal Entity Name": "Vaughantrode Limited",
    "Directors": [
      "Victor Clark",
      "Wesley Hameldon"
    ],
    "Company Number": "1833050",
    "Principal Place of Business": [
      "7 Appleby Ave",
      "Hilton",
      "Simmons City",
      "5934"
    ],
    "NZBN": "9429127096146",
    "Email": "sales@vaughantrode.co.nz",
    "Start Date": "17/07/2014",
    "Location": "Simmons City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.beckettnetworks.net",
    "Registered Address": [
      "7 Young St",
      "Varley",
      "Reynoldstown",
      "6271"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "7 Young St",
      "Varley",
      "Reynoldstown",
      "6271"
    ],
    "Phone Number": "+64 14 6730343",
    "Legal Entity Name": "Beckett Networks Limited",
    "Directors": [
      "Staci Armistead",
      "Sheldon Eads",
      "Ignatius Kimball",
      "Joshua Treloar",
      "Natalie Foss",
      "Howard Kersey"
    ],
    "Company Number": "1833337",
    "Principal Place of Business": [
      "93 Pocock St",
      "Bonham",
      "Hameldonville",
      "5733"
    ],
    "NZBN": "9429127100737",
    "Email": "info@beckettnetworks.net",
    "Start Date": "26/01/1998",
    "Location": "Hameldonville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.stjohnsolutions.com",
    "Registered Address": [
      "54g Bass St",
      "Burke",
      "Palmerton",
      "6230"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Film and television",
    "Address for Service": [
      "54g Bass St",
      "Burke",
      "Palmerton",
      "6230"
    ],
    "Phone Number": "+64 29 5864713",
    "Legal Entity Name": "St John Solutions Limited",
    "Directors": [
      "Parker Thacker",
      "Oswald Bailey",
      "Kasa Ibbott"
    ],
    "Company Number": "1833755",
    "Principal Place of Business": [
      "54g Bass St",
      "Burke",
      "Palmerton",
      "6230"
    ],
    "NZBN": "9429127103646",
    "Email": "info@stjohnsolutions.com",
    "Start Date": "21/09/2002",
    "Location": "Palmerton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.wrayandchristopher.co.nz",
    "Registered Address": [
      "25 Vipond Street",
      "",
      "Boothmanton",
      "3090"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Bloxamsoftware",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "29b Northrop St",
      "",
      "Keenville",
      "3231"
    ],
    "Phone Number": "+64 11 5717444",
    "Legal Entity Name": "Wray and Christopher Limited",
    "Directors": [
      "Andrew Mann",
      "Luke Steed",
      "Gregory Bishop",
      "Sean Arthurson",
      "Cody Yoxall",
      "Scott Alfredson"
    ],
    "Company Number": "1833830",
    "Principal Place of Business": [
      "61 Jerome Rd",
      "Ingram",
      "Cotterill City",
      "1304"
    ],
    "NZBN": "9429127106500",
    "Email": "info@wrayandchristopher.co.nz",
    "Start Date": "03/09/1985",
    "Location": "Cotterill City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.aaa.nz",
    "Registered Address": [
      "74 Stenet Parade",
      "Merricks",
      "Merchantville",
      "3115"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Watkins and Armistead",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "74 Stenet Parade",
      "Merricks",
      "Merchantville",
      "3115"
    ],
    "Phone Number": "+64 17 7789459",
    "Legal Entity Name": "AAA Limited",
    "Directors": [
      "Nicholas Blake"
    ],
    "Company Number": "1834273",
    "Principal Place of Business": [
      "74 Stenet Parade",
      "Merricks",
      "Merchantville",
      "3115"
    ],
    "NZBN": "9429127112266",
    "Email": "admin@aaa.nz",
    "Start Date": "16/04/2001",
    "Location": "Merchantville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.christisonroyleandtollemache.net",
    "Registered Address": [
      "9/244 Beckham Rd",
      "",
      "Wyndham City",
      "3346"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Christison, Royle and Tollemache",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "37j Smalls Street",
      "Samuels",
      "Herbertland",
      "4683"
    ],
    "Phone Number": "+64 18 5208370",
    "Legal Entity Name": "Christison, Royle and Tollemache Limited",
    "Directors": [
      "Val Atterberry",
      "Thea Crewe",
      "Cody Adamson",
      "Marjorie Dwerryhouse",
      "Christa Gorbold",
      "Candice Sudworth"
    ],
    "Company Number": "1835147",
    "Principal Place of Business": [
      "37j Smalls Street",
      "Samuels",
      "Herbertland",
      "4683"
    ],
    "NZBN": "9429127119944",
    "Email": "admin@christisonroyleandtollemache.net",
    "Start Date": "10/07/2004",
    "Location": "Herbertland"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.whittemoretrode.co.nz",
    "Registered Address": [
      "65 Chance Rd",
      "Smythe",
      "Thomson City",
      "1616"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Whittemoretrode",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "65 Chance Rd",
      "Smythe",
      "Thomson City",
      "1616"
    ],
    "Phone Number": "+64 28 5475088",
    "Legal Entity Name": "Whittemoretrode Limited",
    "Directors": [
      "Zane Elmerson",
      "Ian Alvey",
      "Marica Strudwick"
    ],
    "Company Number": "1836053",
    "Principal Place of Business": [
      "19 Knaggs Ave",
      "",
      "Ikin",
      "4684"
    ],
    "NZBN": "9429127126140",
    "Email": "office@whittemoretrode.co.nz",
    "Start Date": "17/09/2004",
    "Location": "Ikin"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.plankandgreen.net",
    "Registered Address": [
      "42 Stainthorpe Parade",
      "Baldwin",
      "Tailorton",
      "2021"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "29 Freeman Parade",
      "Dennell",
      "Pattersonton",
      "6027"
    ],
    "Phone Number": "+64 29 8566445",
    "Legal Entity Name": "Plank and Green Limited",
    "Directors": [
      "Luke Bolton",
      "Amelia Raynerson"
    ],
    "Company Number": "1836407",
    "Principal Place of Business": [
      "64 Rayne Rd",
      "Kitchens",
      "Samuels",
      "7845"
    ],
    "NZBN": "9429127134343",
    "Email": "sales@plankandgreen.net",
    "Start Date": "10/06/1990",
    "Location": "Samuels"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.mann.nz",
    "Registered Address": [
      "13 Harrell Avenue",
      "Stephenson",
      "Huddleston",
      "4256"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Mann",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "13 Harrell Avenue",
      "Stephenson",
      "Huddleston",
      "4256"
    ],
    "Phone Number": "+64 4 6014613",
    "Legal Entity Name": "Mann Limited",
    "Directors": [
      "Flynn Bonham",
      "Timothy Steffen",
      "Coral Poole",
      "Rimona Marston"
    ],
    "Company Number": "1837303",
    "Principal Place of Business": [
      "64 Carl St",
      "",
      "Low",
      "1721"
    ],
    "NZBN": "9429127134886",
    "Email": "info@mann.nz",
    "Start Date": "10/02/1997",
    "Location": "Low"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.easonscope.nz",
    "Registered Address": [
      "4 Parker St",
      "",
      "Hudnalltown",
      "8312"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Easonscope",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "73 Brownlow Rd",
      "Yoxall",
      "Stephensonville",
      "2626"
    ],
    "Phone Number": "+64 4 8280704",
    "Legal Entity Name": "Easonscope Limited",
    "Directors": [
      "Lenice Wootton",
      "Sarala Boyce",
      "Pippa Hopkins",
      "Claudia Northrop"
    ],
    "Company Number": "1838302",
    "Principal Place of Business": [
      "73 Brownlow Rd",
      "Yoxall",
      "Stephensonville",
      "2626"
    ],
    "NZBN": "9429127137337",
    "Email": "office@easonscope.nz",
    "Start Date": "05/07/2009",
    "Location": "Stephensonville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.hobbesnetworks.net",
    "Registered Address": [
      "72 Strudwick Street",
      "Bass",
      "Pryorton",
      "7651"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "72 Strudwick Street",
      "Bass",
      "Pryorton",
      "7651"
    ],
    "Phone Number": "+64 9 8189545",
    "Legal Entity Name": "Hobbes Networks Limited",
    "Directors": [
      "Mindy Heath",
      "Eli Weekes",
      "Traci Watkins",
      "Cory Hallman",
      "Brendan Broadbent",
      "Sabrina Albinson"
    ],
    "Company Number": "1838354",
    "Principal Place of Business": [
      "72 Strudwick Street",
      "Bass",
      "Pryorton",
      "7651"
    ],
    "NZBN": "9429127141990",
    "Email": "antheia@hobbesnetworks.net",
    "Start Date": "27/05/1989",
    "Location": "Pryorton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.peteradkinsandway.kiwi",
    "Registered Address": [
      "31/63j Marchand Parade",
      "Neville",
      "Lowton",
      "3963"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Peter, Adkins and Way",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "31/63j Marchand Parade",
      "Neville",
      "Lowton",
      "3963"
    ],
    "Phone Number": "+64 25 7359993",
    "Legal Entity Name": "Peter, Adkins and Way Limited",
    "Directors": [
      "Conner Quigley",
      "Janet Hermanson",
      "Jinny Winston"
    ],
    "Company Number": "1838441",
    "Principal Place of Business": [
      "31/63j Marchand Parade",
      "Neville",
      "Lowton",
      "3963"
    ],
    "NZBN": "9429127146438",
    "Email": "admin@peteradkinsandway.kiwi",
    "Start Date": "15/07/1990",
    "Location": "Lowton"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.herbertson.co.nz",
    "Registered Address": [
      "50 Belcher Parade",
      "",
      "Thomson",
      "2871"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "21 Ecclestone Ave",
      "",
      "Georgesonton",
      "8151"
    ],
    "Phone Number": "+64 5 6648626",
    "Legal Entity Name": "Herbertson Limited",
    "Directors": [
      "Sawyer Trask",
      "Nigel House"
    ],
    "Company Number": "1839142",
    "Principal Place of Business": [
      "50 Belcher Parade",
      "",
      "Thomson",
      "2871"
    ],
    "NZBN": "9429127154419",
    "Email": "info@herbertson.co.nz",
    "Start Date": "16/02/1992",
    "Location": "Thomson"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.arthurlands.nz",
    "Registered Address": [
      "32 Pelley Rd",
      "",
      "Cannon City",
      "6261"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "5f Ott Rd",
      "Burton",
      "Bunkertown",
      "3644"
    ],
    "Phone Number": "+64 24 5242012",
    "Legal Entity Name": "Arthur Lands Limited",
    "Directors": [
      "Thomas Bloxam",
      "Tracy Black",
      "Kurt Patterson"
    ],
    "Company Number": "1839195",
    "Principal Place of Business": [
      "32 Pelley Rd",
      "",
      "Cannon City",
      "6261"
    ],
    "NZBN": "9429127163411",
    "Email": "info@arthurlands.nz",
    "Start Date": "02/02/2005",
    "Location": "Cannon City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.oonln.nz",
    "Registered Address": [
      "82 Queen Avenue",
      "Boivin",
      "Baker",
      "4798"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Denman Experience",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "47/23i Trengove Rd",
      "Pelley",
      "Tittensorton",
      "3297"
    ],
    "Phone Number": "+64 15 8919947",
    "Legal Entity Name": "OONLN Limited",
    "Directors": [
      "Vernon Stoddard"
    ],
    "Company Number": "1839434",
    "Principal Place of Business": [
      "20 Hartell Street",
      "Hurst",
      "Rugglesland",
      "8133"
    ],
    "NZBN": "9429127165200",
    "Email": "klaudia@oonln.nz",
    "Start Date": "29/09/1994",
    "Location": "Rugglesland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.megaporternetworks.co.nz",
    "Registered Address": [
      "29 Jarvis Parade",
      "Pemberton",
      "Owstontown",
      "7448"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "21 Edwardson Rd",
      "",
      "Horsfalltown",
      "1200"
    ],
    "Phone Number": "+64 4 5543322",
    "Legal Entity Name": "Megaporter Networks Limited",
    "Directors": [
      "Kimberlee Britton",
      "Oswald Spearing",
      "Thomas Boothman",
      "Frederick Gibbs"
    ],
    "Company Number": "1839618",
    "Principal Place of Business": [
      "21 Edwardson Rd",
      "",
      "Horsfalltown",
      "1200"
    ],
    "NZBN": "9429127168973",
    "Email": "admin@megaporternetworks.co.nz",
    "Start Date": "11/05/2005",
    "Location": "Horsfalltown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.gardynerandrupertson.com",
    "Registered Address": [
      "36m Clawson St",
      "",
      "Warrenville",
      "1695"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "77 Bond Ave",
      "Rounds",
      "Symonsland",
      "6295"
    ],
    "Phone Number": "+64 11 8122780",
    "Legal Entity Name": "Gardyner and Rupertson Limited",
    "Directors": [
      "Rosaleen Owston",
      "Brandon Sims",
      "Scott Sumner",
      "Georgette Auttenberg",
      "Phillis Harlow",
      "Luke Bennet",
      "Patrice Grant",
      "Hans Wheelock"
    ],
    "Company Number": "1840560",
    "Principal Place of Business": [
      "36m Clawson St",
      "",
      "Warrenville",
      "1695"
    ],
    "NZBN": "9429127174349",
    "Email": "info@gardynerandrupertson.com",
    "Start Date": "26/10/1993",
    "Location": "Warrenville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.aol.nz",
    "Registered Address": [
      "80p Wolfe St",
      "Coombs",
      "Andrewson City",
      "8468"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Turnbull, Clark and Lum",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "26 Tobias Highway",
      "",
      "Low",
      "8337"
    ],
    "Phone Number": "+64 7 6454033",
    "Legal Entity Name": "AOL Limited",
    "Directors": [
      "Aideen Chamberlain"
    ],
    "Company Number": "1840764",
    "Principal Place of Business": [
      "83 Long Rd",
      "",
      "Kendalltown",
      "5805"
    ],
    "NZBN": "9429127181781",
    "Email": "info@aol.nz",
    "Start Date": "19/12/1999",
    "Location": "Kendalltown"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.awab.com",
    "Registered Address": [
      "76 Heath Avenue",
      "",
      "Garrodton",
      "7423"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "51 Victors Rd",
      "Saunders",
      "Benbowtown",
      "2212"
    ],
    "Phone Number": "+64 16 8498189",
    "Legal Entity Name": "AWAB Limited",
    "Directors": [
      "Lesley Greene",
      "Juliet Martins",
      "Grace Wash",
      "Richard Timothyson",
      "Joseph Bonney"
    ],
    "Company Number": "1841258",
    "Principal Place of Business": [
      "38 Cristians St",
      "",
      "Thomson",
      "3635"
    ],
    "NZBN": "9429127188186",
    "Email": "michael@awab.com",
    "Start Date": "09/12/1998",
    "Location": "Thomson"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.tre.net",
    "Registered Address": [
      "61 Alfredson St",
      "",
      "Cooksonland",
      "5490"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "TRE",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "61 Alfredson St",
      "",
      "Cooksonland",
      "5490"
    ],
    "Phone Number": "+64 6 8823340",
    "Legal Entity Name": "TRE Limited",
    "Directors": [
      "Joshua Harper",
      "Christopher Ashworth",
      "Lenice Robertson"
    ],
    "Company Number": "1841451",
    "Principal Place of Business": [
      "61 Alfredson St",
      "",
      "Cooksonland",
      "5490"
    ],
    "NZBN": "9429127193883",
    "Email": "office@tre.net",
    "Start Date": "07/11/2010",
    "Location": "Cooksonland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.arkwrightandstroud.co.nz",
    "Registered Address": [
      "78j Blackman Street",
      "",
      "Dickensonton",
      "5935"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "78j Blackman Street",
      "",
      "Dickensonton",
      "5935"
    ],
    "Phone Number": "+64 11 5629623",
    "Legal Entity Name": "Arkwright and Stroud Limited",
    "Directors": [
      "Jasmin Danell",
      "Theresa Poole",
      "Virgil Pocock",
      "Orson Harvey",
      "Xander Wyndham",
      "Antonella Randall"
    ],
    "Company Number": "1841660",
    "Principal Place of Business": [
      "48/31 Holt Ave",
      "",
      "Salomonville",
      "6278"
    ],
    "NZBN": "9429127195047",
    "Email": "office@arkwrightandstroud.co.nz",
    "Start Date": "01/08/2001",
    "Location": "Salomonville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.leachmilburnanddavison.net",
    "Registered Address": [
      "50 Earlson Rd",
      "Hancock",
      "Burnhamtown",
      "2817"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "74 Honeysett Highway",
      "",
      "Whitakerville",
      "7561"
    ],
    "Phone Number": "+64 29 8992223",
    "Legal Entity Name": "Leach, Milburn and Davison Limited",
    "Directors": [
      "Kurt Orman",
      "Irena Normanson",
      "Brandon Teel",
      "Quinn Janson"
    ],
    "Company Number": "1842540",
    "Principal Place of Business": [
      "446 Traver Rd",
      "",
      "Eccleston",
      "5646"
    ],
    "NZBN": "9429127203513",
    "Email": "sales@leachmilburnanddavison.net",
    "Start Date": "01/12/1998",
    "Location": "Eccleston"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.prescott.nz",
    "Registered Address": [
      "39 Jinks Road",
      "Traver",
      "Gloverville",
      "7961"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Prescott",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "39 Jinks Road",
      "Traver",
      "Gloverville",
      "7961"
    ],
    "Phone Number": "+64 22 7439505",
    "Legal Entity Name": "Prescott Limited",
    "Directors": [
      "Amber Hodson",
      "Brian Madison"
    ],
    "Company Number": "1843478",
    "Principal Place of Business": [
      "61 Hunter Street",
      "Aiken",
      "Auttenberg City",
      "2482"
    ],
    "NZBN": "9429127206125",
    "Email": "whitney@prescott.nz",
    "Start Date": "01/10/2014",
    "Location": "Auttenberg City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.crp.nz",
    "Registered Address": [
      "48 Ryeley Ave",
      "Burton",
      "Bradleyton",
      "1996"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "CRP",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "1 Disney Ave",
      "",
      "Barkerton",
      "7919"
    ],
    "Phone Number": "+64 8 5898865",
    "Legal Entity Name": "CRP Limited",
    "Directors": [
      "Faith Martins",
      "Gabriella Haywood",
      "Amy Washington",
      "Warren Bailey",
      "Michael Paterson"
    ],
    "Company Number": "1844001",
    "Principal Place of Business": [
      "85 Salvage Highway",
      "",
      "Pettigrew City",
      "8629"
    ],
    "NZBN": "9429127212942",
    "Email": "admin@crp.nz",
    "Start Date": "03/01/1986",
    "Location": "Pettigrew City"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.symonstech.co.nz",
    "Registered Address": [
      "83 Arthurson Parade",
      "",
      "Elytown",
      "3171"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Symonstech",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "41 Ethans Ave",
      "Gully",
      "Parentton",
      "7563"
    ],
    "Phone Number": "+64 11 8905446",
    "Legal Entity Name": "Symonstech Limited",
    "Directors": [
      "Flynn Quincey",
      "Jo Leonardson",
      "Preston Witherspoon",
      "Zane Haight",
      "Katy Malone",
      "Kurt Jepson",
      "Kevin Hopper"
    ],
    "Company Number": "1844111",
    "Principal Place of Business": [
      "82 Pierson Ave",
      "",
      "Danielltown",
      "8568"
    ],
    "NZBN": "9429127222590",
    "Email": "office@symonstech.co.nz",
    "Start Date": "29/01/2006",
    "Location": "Danielltown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.aas.co.nz",
    "Registered Address": [
      "66 Savidge Highway",
      "",
      "Fostertown",
      "1515"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "BWB",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "71r Saunders Road",
      "Jeffers",
      "Hensonland",
      "5625"
    ],
    "Phone Number": "+64 26 8202705",
    "Legal Entity Name": "AAS Limited",
    "Directors": [
      "Zane Gardner",
      "Angelica Fabian",
      "Andra Robert",
      "Sean Weaver",
      "Zane Marlow",
      "Derek Stevenson"
    ],
    "Company Number": "1844840",
    "Principal Place of Business": [
      "66 Savidge Highway",
      "",
      "Fostertown",
      "1515"
    ],
    "NZBN": "9429127230977",
    "Email": "omar@aas.co.nz",
    "Start Date": "15/08/1991",
    "Location": "Fostertown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.gardynersolutions.nz",
    "Registered Address": [
      "70 Norwood St",
      "",
      "Hopper City",
      "2001"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Gardyner Solutions",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "70 Norwood St",
      "",
      "Hopper City",
      "2001"
    ],
    "Phone Number": "+64 27 5364479",
    "Legal Entity Name": "Gardyner Solutions Limited",
    "Directors": [
      "Sharon Mark"
    ],
    "Company Number": "1845633",
    "Principal Place of Business": [
      "70 Norwood St",
      "",
      "Hopper City",
      "2001"
    ],
    "NZBN": "9429127237419",
    "Email": "sales@gardynersolutions.nz",
    "Start Date": "11/07/1988",
    "Location": "Hopper City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.botwrightlands.co.nz",
    "Registered Address": [
      "15 Keys St",
      "",
      "Cheshireland",
      "2143"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "69 Yap Ave",
      "Sandford",
      "Honeycuttville",
      "3950"
    ],
    "Phone Number": "+64 6 7707755",
    "Legal Entity Name": "Botwright Lands Limited",
    "Directors": [
      "Sheela Varley",
      "Pamela Bourke",
      "Kimberlee Fox",
      "Christa Rowntree",
      "Bernita Shakesheave",
      "Luke Maddison",
      "Pauline Traver",
      "Preston Sandford"
    ],
    "Company Number": "1845726",
    "Principal Place of Business": [
      "96 Polley Road",
      "",
      "Rimmer",
      "7459"
    ],
    "NZBN": "9429127244240",
    "Email": "admin@botwrightlands.co.nz",
    "Start Date": "17/07/1996",
    "Location": "Rimmer"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.sargentsolutions.co.nz",
    "Registered Address": [
      "58 Honeysett Street",
      "Fear",
      "Sargent City",
      "5624"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Sargent Solutions",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "76b Bartram Highway",
      "",
      "Evanson City",
      "4662"
    ],
    "Phone Number": "+64 8 6617524",
    "Legal Entity Name": "Sargent Solutions Limited",
    "Directors": [
      "Mary Southers",
      "Gertrud Quick"
    ],
    "Company Number": "1845780",
    "Principal Place of Business": [
      "11/51 Chancellor Street",
      "",
      "Rogersontown",
      "3196"
    ],
    "NZBN": "9429127251637",
    "Email": "office@sargentsolutions.co.nz",
    "Start Date": "05/06/2003",
    "Location": "Rogersontown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.huffandking.co.nz",
    "Registered Address": [
      "23w Sempers Ave",
      "Botwright",
      "Watson",
      "2921"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "94n Henryson Avenue",
      "",
      "Christianston",
      "5669"
    ],
    "Phone Number": "+64 7 7875815",
    "Legal Entity Name": "Huff and King Limited",
    "Directors": [
      "Peggy Christians",
      "Aesha Bartram",
      "Daniel Thomson"
    ],
    "Company Number": "1846244",
    "Principal Place of Business": [
      "16 Ash Ave",
      "Hunt",
      "Mitchell City",
      "2125"
    ],
    "NZBN": "9429127256977",
    "Email": "office@huffandking.co.nz",
    "Start Date": "21/02/2001",
    "Location": "Mitchell City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.eesns.co.nz",
    "Registered Address": [
      "26/26 Mounce St",
      "Brassington",
      "Georgesontown",
      "1715"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "26/26 Mounce St",
      "Brassington",
      "Georgesontown",
      "1715"
    ],
    "Phone Number": "+64 13 7670101",
    "Legal Entity Name": "EESNS Limited",
    "Directors": [
      "Richard Preston",
      "Hans Henderson",
      "Becca Rogers",
      "Matthew Dickenson",
      "Helana Sudworth",
      "Eli Hargrave",
      "Quincy Wash"
    ],
    "Company Number": "1846323",
    "Principal Place of Business": [
      "26/26 Mounce St",
      "Brassington",
      "Georgesontown",
      "1715"
    ],
    "NZBN": "9429127259329",
    "Email": "sales@eesns.co.nz",
    "Start Date": "17/06/1990",
    "Location": "Georgesontown"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.draperandstrudwick.net",
    "Registered Address": [
      "52 Hargrave Rd",
      "Honeysett",
      "Mallorytown",
      "3488"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "82 Bryson Street",
      "Nicholson",
      "Rakes",
      "6942"
    ],
    "Phone Number": "+64 25 7566823",
    "Legal Entity Name": "Draper and Strudwick Limited",
    "Directors": [
      "Logan Overton"
    ],
    "Company Number": "1846585",
    "Principal Place of Business": [
      "72x Ethans Ave",
      "Curtis",
      "Espenson",
      "6398"
    ],
    "NZBN": "9429127262671",
    "Email": "sales@draperandstrudwick.net",
    "Start Date": "27/01/1995",
    "Location": "Espenson"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ven.nz",
    "Registered Address": [
      "44 Newell St",
      "",
      "Rileyton",
      "6990"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "44 Newell St",
      "",
      "Rileyton",
      "6990"
    ],
    "Phone Number": "+64 11 7664238",
    "Legal Entity Name": "VEN Limited",
    "Directors": [
      "Troy Lyon"
    ],
    "Company Number": "1847400",
    "Principal Place of Business": [
      "44 Newell St",
      "",
      "Rileyton",
      "6990"
    ],
    "NZBN": "9429127264514",
    "Email": "info@ven.nz",
    "Start Date": "01/07/2010",
    "Location": "Rileyton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.kersey.co.nz",
    "Registered Address": [
      "22/82 Madison Avenue",
      "",
      "Northropland",
      "1390"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "22/82 Madison Avenue",
      "",
      "Northropland",
      "1390"
    ],
    "Phone Number": "+64 18 6261632",
    "Legal Entity Name": "Kersey Limited",
    "Directors": [
      "Ishmael Tash",
      "Ignatius Trevis",
      "Robert Ness",
      "Lottie Peter",
      "Becca Sherburne",
      "Melanie Hume",
      "Karla James"
    ],
    "Company Number": "1847451",
    "Principal Place of Business": [
      "22/82 Madison Avenue",
      "",
      "Northropland",
      "1390"
    ],
    "NZBN": "9429127272892",
    "Email": "trenton@kersey.co.nz",
    "Start Date": "30/11/2008",
    "Location": "Northropland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.wortham.net",
    "Registered Address": [
      "40 Ness Rd",
      "",
      "Nathanson",
      "1788"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Film and television",
    "Address for Service": [
      "34/12 Babcocke St",
      "",
      "Rupertsonton",
      "4525"
    ],
    "Phone Number": "+64 11 5498205",
    "Legal Entity Name": "Wortham Limited",
    "Directors": [
      "Valerie King",
      "Ruben Peter",
      "Abaigeal Wilkinson",
      "Yuri Carter",
      "Xavier Edwards"
    ],
    "Company Number": "1847789",
    "Principal Place of Business": [
      "3 Victorson St",
      "",
      "Burton",
      "5612"
    ],
    "NZBN": "9429127277613",
    "Email": "info@wortham.net",
    "Start Date": "12/08/1993",
    "Location": "Burton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.traviss.nz",
    "Registered Address": [
      "24/46 Hall Street",
      "Verity",
      "Sherburneton",
      "2044"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Breckenridgetrode",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "70 Jacobs Avenue",
      "Appleton",
      "Hornetown",
      "3018"
    ],
    "Phone Number": "+64 22 8605450",
    "Legal Entity Name": "Traviss Limited",
    "Directors": [
      "Elizabeth Revie",
      "Veronique Spalding",
      "Dylan Studwick",
      "Oswald Armistead",
      "Gerald Avery"
    ],
    "Company Number": "1848366",
    "Principal Place of Business": [
      "99p Fay Street",
      "",
      "Carpenter City",
      "5659"
    ],
    "NZBN": "9429127278337",
    "Email": "wyatt@traviss.nz",
    "Start Date": "18/02/2003",
    "Location": "Carpenter City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.lucasscape.nz",
    "Registered Address": [
      "69d Mills St",
      "",
      "Whittleton",
      "7465"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Interholmes",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "69d Mills St",
      "",
      "Whittleton",
      "7465"
    ],
    "Phone Number": "+64 19 6960323",
    "Legal Entity Name": "Lucasscape Limited",
    "Directors": [
      "Dylan Treloar",
      "Leigh Mason",
      "Georgie Weekes",
      "Aubrey Gabrielson",
      "Philip Alexander"
    ],
    "Company Number": "1848551",
    "Principal Place of Business": [
      "69d Mills St",
      "",
      "Whittleton",
      "7465"
    ],
    "NZBN": "9429127283676",
    "Email": "sales@lucasscape.nz",
    "Start Date": "16/11/2013",
    "Location": "Whittleton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.iyi.nz",
    "Registered Address": [
      "79 Malone Parade",
      "",
      "Elliston",
      "4656"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Intergibbstrode",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "57 Dixon Ave",
      "Womack",
      "Hume",
      "5296"
    ],
    "Phone Number": "+64 12 6016818",
    "Legal Entity Name": "IYI Limited",
    "Directors": [
      "Roland Speight",
      "Rosalie Horsfall",
      "Blake Rogerson",
      "Jocelyn Attaway"
    ],
    "Company Number": "1849144",
    "Principal Place of Business": [
      "57 Dixon Ave",
      "Womack",
      "Hume",
      "5296"
    ],
    "NZBN": "9429127292166",
    "Email": "sales@iyi.nz",
    "Start Date": "10/04/1994",
    "Location": "Hume"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.victorandbrooks.net.nz",
    "Registered Address": [
      "30 Tolbert Road",
      "Milton",
      "Bloodworth",
      "8713"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "30 Tolbert Road",
      "Milton",
      "Bloodworth",
      "8713"
    ],
    "Phone Number": "+64 23 6279030",
    "Legal Entity Name": "Victor and Brooks Limited",
    "Directors": [
      "Henry Crawford",
      "Floyd Ellison",
      "Henrietta Stamp",
      "Bernita Revie"
    ],
    "Company Number": "1849328",
    "Principal Place of Business": [
      "982 Caulfield Ave",
      "Traylor",
      "Dexterton",
      "1208"
    ],
    "NZBN": "9429127297000",
    "Email": "office@victorandbrooks.net.nz",
    "Start Date": "15/04/1994",
    "Location": "Dexterton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ranr.nz",
    "Registered Address": [
      "12/40 Dennel Rd",
      "Poole",
      "Crouchton",
      "7217"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "RANR",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "2 Howland Ave",
      "Ruggles",
      "Kirbyton",
      "5044"
    ],
    "Phone Number": "+64 11 5898372",
    "Legal Entity Name": "RANR Limited",
    "Directors": [
      "Nicholas Mathews",
      "Brian Jinks",
      "Yves Christisen",
      "Katie Gregory",
      "Rhea Marlow",
      "Roland Abrahams"
    ],
    "Company Number": "1850152",
    "Principal Place of Business": [
      "25/81 Hunnisett St",
      "",
      "Hawkland",
      "6120"
    ],
    "NZBN": "9429127299325",
    "Email": "office@ranr.nz",
    "Start Date": "14/01/2002",
    "Location": "Hawkland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.wheelerscape.nz",
    "Registered Address": [
      "97 Witherspoon St",
      "",
      "Hanson",
      "6546"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "97 Witherspoon St",
      "",
      "Hanson",
      "6546"
    ],
    "Phone Number": "+64 19 6046223",
    "Legal Entity Name": "Wheelerscape Limited",
    "Directors": [
      "Yves Fairbairn",
      "Amiela Morriss",
      "Viki Fairbairn",
      "Savina Travis"
    ],
    "Company Number": "1850290",
    "Principal Place of Business": [
      "100 Alvey Rd",
      "Keen",
      "Christisonville",
      "5132"
    ],
    "NZBN": "9429127306955",
    "Email": "office@wheelerscape.nz",
    "Start Date": "19/10/2006",
    "Location": "Christisonville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.hermansonandhepburn.co.nz",
    "Registered Address": [
      "8d Rier Ave",
      "Cartwright",
      "Disneyton",
      "1866"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Stevenson",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "8d Rier Ave",
      "Cartwright",
      "Disneyton",
      "1866"
    ],
    "Phone Number": "+64 16 6015505",
    "Legal Entity Name": "Hermanson and Hepburn Limited",
    "Directors": [
      "Savina Draper",
      "Rachel Stringer",
      "Kristie Murgatroyd"
    ],
    "Company Number": "1851280",
    "Principal Place of Business": [
      "60 Howse St",
      "Williams",
      "Robinson City",
      "4456"
    ],
    "NZBN": "9429127307518",
    "Email": "info@hermansonandhepburn.co.nz",
    "Start Date": "22/01/1996",
    "Location": "Robinson City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.megaplaskettsoftware.co.nz",
    "Registered Address": [
      "53 Goode Street",
      "Outlaw",
      "Garrardville",
      "1950"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "43/752 Gilliam Street",
      "Sandford",
      "Milton",
      "6701"
    ],
    "Phone Number": "+64 26 7271540",
    "Legal Entity Name": "Megaplaskettsoftware Limited",
    "Directors": [
      "Vaughn Southgate",
      "Allie Fuller",
      "Flora Skinner",
      "Nicholas Downer",
      "Hans Parker",
      "Frederick Booner",
      "Jill Thorn"
    ],
    "Company Number": "1851346",
    "Principal Place of Business": [
      "53 Goode Street",
      "Outlaw",
      "Garrardville",
      "1950"
    ],
    "NZBN": "9429127314301",
    "Email": "sales@megaplaskettsoftware.co.nz",
    "Start Date": "19/11/2013",
    "Location": "Garrardville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.gardner.nz",
    "Registered Address": [
      "55 Pitts Rd",
      "Key",
      "Bondton",
      "2057"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "55 Pitts Rd",
      "Key",
      "Bondton",
      "2057"
    ],
    "Phone Number": "+64 17 8133603",
    "Legal Entity Name": "Gardner Limited",
    "Directors": [
      "Elizabeth Fox",
      "Ignatius Royce",
      "Monica Derricks",
      "Vincent Shakesheave",
      "Kurt Winston",
      "Newman Preston",
      "Steven Ramsey"
    ],
    "Company Number": "1852249",
    "Principal Place of Business": [
      "98x Corra Street",
      "Sauvage",
      "Earl",
      "6425"
    ],
    "NZBN": "9429127319405",
    "Email": "wilfred@gardner.nz",
    "Start Date": "12/02/2000",
    "Location": "Earl"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.badcockeottandsymons.nz",
    "Registered Address": [
      "76 Peterson Rd",
      "",
      "Atteberryville",
      "7361"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Badcocke, Ott and Symons",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "56 Rimmer Avenue",
      "Ellsworth",
      "Dicksonville",
      "5966"
    ],
    "Phone Number": "+64 17 8817390",
    "Legal Entity Name": "Badcocke, Ott and Symons Limited",
    "Directors": [
      "Scott Corra",
      "Kami Bristow",
      "Vaughn Bancroft",
      "Diane Dennel",
      "Parker Upton",
      "Jocelyn Arnold"
    ],
    "Company Number": "1852516",
    "Principal Place of Business": [
      "56 Rimmer Avenue",
      "Ellsworth",
      "Dicksonville",
      "5966"
    ],
    "NZBN": "9429127321743",
    "Email": "info@badcockeottandsymons.nz",
    "Start Date": "01/01/1995",
    "Location": "Dicksonville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.hudsonexperience.nz",
    "Registered Address": [
      "47 Miles Parade",
      "",
      "Averyton",
      "8644"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Hudson Experience",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "47 Miles Parade",
      "",
      "Averyton",
      "8644"
    ],
    "Phone Number": "+64 23 7993833",
    "Legal Entity Name": "Hudson Experience Limited",
    "Directors": [
      "Eli Jephson",
      "Annaliese Banks",
      "Hans St John",
      "Hal Marlow",
      "Jaleen Gray",
      "Richard Morce",
      "Selena Neville"
    ],
    "Company Number": "1852778",
    "Principal Place of Business": [
      "47 Miles Parade",
      "",
      "Averyton",
      "8644"
    ],
    "NZBN": "9429127327653",
    "Email": "admin@hudsonexperience.nz",
    "Start Date": "31/10/1995",
    "Location": "Averyton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.christiansonandhardwick.net",
    "Registered Address": [
      "29a Blakeslee Rd",
      "",
      "Hathewayton",
      "8754"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Christianson and Hardwick",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "8k Watkins Rd",
      "Boone",
      "Rennoldtown",
      "2981"
    ],
    "Phone Number": "+64 18 5312044",
    "Legal Entity Name": "Christianson and Hardwick Limited",
    "Directors": [
      "Abbey Dannel"
    ],
    "Company Number": "1853262",
    "Principal Place of Business": [
      "8k Watkins Rd",
      "Boone",
      "Rennoldtown",
      "2981"
    ],
    "NZBN": "9429127335146",
    "Email": "admin@christiansonandhardwick.net",
    "Start Date": "30/10/2007",
    "Location": "Rennoldtown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.mnmln.co.nz",
    "Registered Address": [
      "67 Tupper Ave",
      "Thwaite",
      "Yorkton",
      "1606"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "MNMLN",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "75 Rounds Road",
      "",
      "Dwerryhousetown",
      "6196"
    ],
    "Phone Number": "+64 15 8646068",
    "Legal Entity Name": "MNMLN Limited",
    "Directors": [
      "Donald Bancroft",
      "Hans Woodhams",
      "William Ryers",
      "Maria Attwater",
      "Trisha Granger"
    ],
    "Company Number": "1853615",
    "Principal Place of Business": [
      "67 Tupper Ave",
      "Thwaite",
      "Yorkton",
      "1606"
    ],
    "NZBN": "9429127341147",
    "Email": "office@mnmln.co.nz",
    "Start Date": "30/11/1996",
    "Location": "Yorkton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.pryor.net",
    "Registered Address": [
      "39/67 Hodson Rd",
      "Comstock",
      "Lomantown",
      "2374"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Pryor",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "31/87 Granger Rd",
      "Steed",
      "Wilkerson City",
      "6115"
    ],
    "Phone Number": "+64 10 7929390",
    "Legal Entity Name": "Pryor Limited",
    "Directors": [
      "Zane Norwood",
      "Fifi Breckinridge",
      "Tia Paulson"
    ],
    "Company Number": "1854391",
    "Principal Place of Business": [
      "31/87 Granger Rd",
      "Steed",
      "Wilkerson City",
      "6115"
    ],
    "NZBN": "9429127343059",
    "Email": "sales@pryor.net",
    "Start Date": "04/07/2001",
    "Location": "Wilkerson City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.gabrielscorp.net.nz",
    "Registered Address": [
      "63 Benton Ave",
      "Davis",
      "Hardwick City",
      "6748"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Gabrielscorp",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "63 Benton Ave",
      "Davis",
      "Hardwick City",
      "6748"
    ],
    "Phone Number": "+64 14 6833965",
    "Legal Entity Name": "Gabrielscorp Limited",
    "Directors": [
      "Daphne Nye",
      "Wilfred Arrington"
    ],
    "Company Number": "1854558",
    "Principal Place of Business": [
      "63 Benton Ave",
      "Davis",
      "Hardwick City",
      "6748"
    ],
    "NZBN": "9429127345992",
    "Email": "sales@gabrielscorp.net.nz",
    "Start Date": "23/10/2000",
    "Location": "Hardwick City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.interhobbs.nz",
    "Registered Address": [
      "5/65 Slater Parade",
      "School",
      "Byrdton",
      "7235"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "5/65 Slater Parade",
      "School",
      "Byrdton",
      "7235"
    ],
    "Phone Number": "+64 16 6460842",
    "Legal Entity Name": "Interhobbs Limited",
    "Directors": [
      "Victor Gilliam"
    ],
    "Company Number": "1854987",
    "Principal Place of Business": [
      "55 Martinson Road",
      "Aiken",
      "Sergeantland",
      "7355"
    ],
    "NZBN": "9429127354741",
    "Email": "office@interhobbs.nz",
    "Start Date": "14/12/1988",
    "Location": "Sergeantland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.upnu.co.nz",
    "Registered Address": [
      "29 Ogden St",
      "",
      "Sims",
      "7127"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "20 Bass St",
      "Harper",
      "Richard",
      "4575"
    ],
    "Phone Number": "+64 21 6683512",
    "Legal Entity Name": "UPNU Limited",
    "Directors": [
      "Romana Richardson",
      "Tania Hyland",
      "Charles Hollins",
      "Fifi Ryers",
      "William Davis",
      "Cybill Sappington"
    ],
    "Company Number": "1855320",
    "Principal Place of Business": [
      "51i Rowntree Ave",
      "Coombs",
      "Wilkinsontown",
      "3268"
    ],
    "NZBN": "9429127355052",
    "Email": "sales@upnu.co.nz",
    "Start Date": "17/12/1998",
    "Location": "Wilkinsontown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.christianssoftware.com",
    "Registered Address": [
      "39f Avery Rd",
      "Cristiansen",
      "Williamsonville",
      "7839"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "6 Dixon Highway",
      "School",
      "Seabrooke City",
      "8719"
    ],
    "Phone Number": "+64 12 7821943",
    "Legal Entity Name": "Christianssoftware Limited",
    "Directors": [
      "Michael Steed",
      "Amy Weaver",
      "Zachary Hardy"
    ],
    "Company Number": "1855703",
    "Principal Place of Business": [
      "39f Avery Rd",
      "Cristiansen",
      "Williamsonville",
      "7839"
    ],
    "NZBN": "9429127355762",
    "Email": "sales@christianssoftware.com",
    "Start Date": "29/10/2011",
    "Location": "Williamsonville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.dyernetworks.net",
    "Registered Address": [
      "4 Stern Street",
      "",
      "Rileytown",
      "1655"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Dyer Networks",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "32 Curtis Ave",
      "",
      "Ackerman",
      "6033"
    ],
    "Phone Number": "+64 25 5336669",
    "Legal Entity Name": "Dyer Networks Limited",
    "Directors": [
      "Keith Bagley",
      "Muriel Spearing"
    ],
    "Company Number": "1856693",
    "Principal Place of Business": [
      "4 Stern Street",
      "",
      "Rileytown",
      "1655"
    ],
    "NZBN": "9429127359951",
    "Email": "office@dyernetworks.net",
    "Start Date": "04/09/2000",
    "Location": "Rileytown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.jefferyandhaggard.co.nz",
    "Registered Address": [
      "4/37 Ott Road",
      "",
      "Easomton",
      "4549"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "4/37 Ott Road",
      "",
      "Easomton",
      "4549"
    ],
    "Phone Number": "+64 27 8547558",
    "Legal Entity Name": "Jeffery and Haggard Limited",
    "Directors": [
      "Patrick Longstaff",
      "Frederick Smalls"
    ],
    "Company Number": "1857288",
    "Principal Place of Business": [
      "4/37 Ott Road",
      "",
      "Easomton",
      "4549"
    ],
    "NZBN": "9429127365037",
    "Email": "harold@jefferyandhaggard.co.nz",
    "Start Date": "12/05/1993",
    "Location": "Easomton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ayw.nz",
    "Registered Address": [
      "89 Ryder Parade",
      "Hamm",
      "Fullerland",
      "4764"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Pierson, Crouch and Victor",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "89 Ryder Parade",
      "Hamm",
      "Fullerland",
      "4764"
    ],
    "Phone Number": "+64 8 7108016",
    "Legal Entity Name": "AYW Limited",
    "Directors": [
      "Andriana Stephenson"
    ],
    "Company Number": "1857737",
    "Principal Place of Business": [
      "636 Barber Parade",
      "",
      "Sessionston",
      "3057"
    ],
    "NZBN": "9429127365730",
    "Email": "admin@ayw.nz",
    "Start Date": "14/01/1997",
    "Location": "Sessionston"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.thornetech.net.nz",
    "Registered Address": [
      "50 Quigley Highway",
      "Godfrey",
      "Readdieland",
      "1496"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "50 Quigley Highway",
      "Godfrey",
      "Readdieland",
      "1496"
    ],
    "Phone Number": "+64 6 8810598",
    "Legal Entity Name": "Thornetech Limited",
    "Directors": [
      "Dawn Carlisle",
      "Nola Walmsley",
      "Madeleine Rake",
      "Paige Chamberlain",
      "Christabel Nathanson",
      "Hugh Ellisson",
      "Omar Alvey",
      "Louise Hunter"
    ],
    "Company Number": "1858051",
    "Principal Place of Business": [
      "50 Quigley Highway",
      "Godfrey",
      "Readdieland",
      "1496"
    ],
    "NZBN": "9429127368731",
    "Email": "office@thornetech.net.nz",
    "Start Date": "04/01/2006",
    "Location": "Readdieland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.boothmanandmerchant.nz",
    "Registered Address": [
      "52 Hicks St",
      "Mathews",
      "Starkton",
      "8500"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "32 Leonardson St",
      "",
      "Tupperton",
      "5401"
    ],
    "Phone Number": "+64 14 5786504",
    "Legal Entity Name": "Boothman and Merchant Limited",
    "Directors": [
      "Karren Thwaite",
      "James Queen",
      "Nadira Brewer"
    ],
    "Company Number": "1858440",
    "Principal Place of Business": [
      "379 Nicolson Ave",
      "Harrison",
      "Deadmanville",
      "2619"
    ],
    "NZBN": "9429127374961",
    "Email": "office@boothmanandmerchant.nz",
    "Start Date": "20/10/2010",
    "Location": "Deadmanville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.iip.net",
    "Registered Address": [
      "91 Strickland Rd",
      "Steele",
      "Archer City",
      "4920"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "IIP",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "67 Horne Ave",
      "Simpson",
      "Chancellortown",
      "8253"
    ],
    "Phone Number": "+64 10 5724735",
    "Legal Entity Name": "IIP Limited",
    "Directors": [
      "Cecily Rowland",
      "Angelica Levitt",
      "Dylan White"
    ],
    "Company Number": "1859259",
    "Principal Place of Business": [
      "67 Horne Ave",
      "Simpson",
      "Chancellortown",
      "8253"
    ],
    "NZBN": "9429127384243",
    "Email": "admin@iip.net",
    "Start Date": "02/12/1987",
    "Location": "Chancellortown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ethansonenterprises.nz",
    "Registered Address": [
      "81 Nicholson St",
      "",
      "Bennet",
      "1407"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "46j Plank Highway",
      "",
      "Barlowville",
      "3072"
    ],
    "Phone Number": "+64 8 8757861",
    "Legal Entity Name": "Ethanson Enterprises Limited",
    "Directors": [
      "Parker Wheeler",
      "Vicki Osbourne",
      "Christopher Pitts"
    ],
    "Company Number": "1859335",
    "Principal Place of Business": [
      "81 Nicholson St",
      "",
      "Bennet",
      "1407"
    ],
    "NZBN": "9429127390596",
    "Email": "info@ethansonenterprises.nz",
    "Start Date": "23/07/2014",
    "Location": "Bennet"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.becketandwatkins.com",
    "Registered Address": [
      "13 Ellsworth Avenue",
      "",
      "Brayville",
      "6731"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "13 Ellsworth Avenue",
      "",
      "Brayville",
      "6731"
    ],
    "Phone Number": "+64 7 6036524",
    "Legal Entity Name": "Becket and Watkins Limited",
    "Directors": [
      "Drew Overton",
      "Elisa Harman",
      "Spencer Sanders"
    ],
    "Company Number": "1859618",
    "Principal Place of Business": [
      "13 Ellsworth Avenue",
      "",
      "Brayville",
      "6731"
    ],
    "NZBN": "9429127398646",
    "Email": "admin@becketandwatkins.com",
    "Start Date": "18/11/2007",
    "Location": "Brayville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.nelsonlands.net.nz",
    "Registered Address": [
      "3 Tate Highway",
      "",
      "Bishoptown",
      "2731"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "27 Jefferson Ave",
      "Thomson",
      "Joneston",
      "2378"
    ],
    "Phone Number": "+64 23 7800353",
    "Legal Entity Name": "Nelson Lands Limited",
    "Directors": [
      "Brady Queshire",
      "Daniel Boone"
    ],
    "Company Number": "1860070",
    "Principal Place of Business": [
      "87 Ryer Street",
      "Becket",
      "Danielsonton",
      "4880"
    ],
    "NZBN": "9429127401711",
    "Email": "kathe@nelsonlands.net.nz",
    "Start Date": "27/12/1987",
    "Location": "Danielsonton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.megarogersonexperience.com",
    "Registered Address": [
      "68 Croft Street",
      "",
      "Williams",
      "6640"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "48 Traver Ave",
      "Gregory",
      "Kiddton",
      "5200"
    ],
    "Phone Number": "+64 13 5574808",
    "Legal Entity Name": "Megarogerson Experience Limited",
    "Directors": [
      "Tessi Traves",
      "Ingrid Tolbert",
      "Irina Donalds",
      "Zachary Wilson",
      "Wilbur Mark",
      "Victor Albinson",
      "Keith Knaggs"
    ],
    "Company Number": "1860611",
    "Principal Place of Business": [
      "68 Croft Street",
      "",
      "Williams",
      "6640"
    ],
    "NZBN": "9429127406778",
    "Email": "admin@megarogersonexperience.com",
    "Start Date": "18/03/1986",
    "Location": "Williams"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.thh.net",
    "Registered Address": [
      "21h Mondy Avenue",
      "Hawkins",
      "Youngeland",
      "5278"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Stidolphscape",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "60 Morrish Highway",
      "",
      "Joiner City",
      "3631"
    ],
    "Phone Number": "+64 20 6500906",
    "Legal Entity Name": "THH Limited",
    "Directors": [
      "Morgan Scott",
      "Paul Easom",
      "Carlana Samson",
      "Abana Allsopp"
    ],
    "Company Number": "1861229",
    "Principal Place of Business": [
      "36b Tash St",
      "",
      "Pearson City",
      "5545"
    ],
    "NZBN": "9429127415268",
    "Email": "office@thh.net",
    "Start Date": "04/03/1989",
    "Location": "Pearson City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.stricklandnewportandtinker.co.nz",
    "Registered Address": [
      "28 Maddison Avenue",
      "",
      "Howlandton",
      "2934"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Strickland, Newport and Tinker",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "3 Senior Rd",
      "Badcocke",
      "Dickensonton",
      "2785"
    ],
    "Phone Number": "+64 13 7680032",
    "Legal Entity Name": "Strickland, Newport and Tinker Limited",
    "Directors": [
      "Patrick Bradford",
      "Eli Hewitt",
      "Thomas Archer",
      "Austin Fleming"
    ],
    "Company Number": "1861924",
    "Principal Place of Business": [
      "28 Maddison Avenue",
      "",
      "Howlandton",
      "2934"
    ],
    "NZBN": "9429127425021",
    "Email": "sales@stricklandnewportandtinker.co.nz",
    "Start Date": "29/09/2012",
    "Location": "Howlandton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.dickemersonanddaniell.co.nz",
    "Registered Address": [
      "65 Draper Highway",
      "Hogarth",
      "Palmer City",
      "8139"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Dick, Emerson and Daniell",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "23/31i Salomon Ave",
      "",
      "Dicktown",
      "8425"
    ],
    "Phone Number": "+64 6 7645381",
    "Legal Entity Name": "Dick, Emerson and Daniell Limited",
    "Directors": [
      "Phillis Hardwick",
      "Zachary Haroldson"
    ],
    "Company Number": "1862851",
    "Principal Place of Business": [
      "65 Draper Highway",
      "Hogarth",
      "Palmer City",
      "8139"
    ],
    "NZBN": "9429127432715",
    "Email": "austin@dickemersonanddaniell.co.nz",
    "Start Date": "09/05/1998",
    "Location": "Palmer City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.shakesheavesoftware.nz",
    "Registered Address": [
      "11/907 Ryley Road",
      "",
      "Witherspoonton",
      "6314"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Shakesheavesoftware",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "85 Andrews St",
      "",
      "Virgoton",
      "2519"
    ],
    "Phone Number": "+64 13 7348537",
    "Legal Entity Name": "Shakesheavesoftware Limited",
    "Directors": [
      "Christel Hodson",
      "Coreen Tuff"
    ],
    "Company Number": "1863722",
    "Principal Place of Business": [
      "9y Ledford Rd",
      "Victorson",
      "Garner",
      "7985"
    ],
    "NZBN": "9429127441410",
    "Email": "office@shakesheavesoftware.nz",
    "Start Date": "21/06/2007",
    "Location": "Garner"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.myerstechnologies.nz",
    "Registered Address": [
      "51 Winter Road",
      "Jackson",
      "Christianston",
      "3912"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Film and television",
    "Address for Service": [
      "28 Sampson Road",
      "Jamison",
      "Simmsville",
      "4311"
    ],
    "Phone Number": "+64 10 6540159",
    "Legal Entity Name": "Myers Technologies Limited",
    "Directors": [
      "Roxana Hunter",
      "Jaleen Myers",
      "Roland St John",
      "Vaughn Huff",
      "Fern Spence"
    ],
    "Company Number": "1864441",
    "Principal Place of Business": [
      "51 Winter Road",
      "Jackson",
      "Christianston",
      "3912"
    ],
    "NZBN": "9429127451006",
    "Email": "riley@myerstechnologies.nz",
    "Start Date": "21/05/1997",
    "Location": "Christianston"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.north.kiwi",
    "Registered Address": [
      "57 Pierson Avenue",
      "Hunter",
      "Stephensonton",
      "6546"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "North",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "36a Hampton Rd",
      "Easom",
      "Harrison City",
      "5110"
    ],
    "Phone Number": "+64 24 8057445",
    "Legal Entity Name": "North Limited",
    "Directors": [
      "Shelly Clifford",
      "Hans Kirby",
      "Edward Ackerman",
      "Margie Mitchell",
      "Grant William",
      "Megan Rounds",
      "Gordon Saunders",
      "Vicki Fry"
    ],
    "Company Number": "1865361",
    "Principal Place of Business": [
      "83 Hext Ave",
      "Crawford",
      "Arkwrighttown",
      "8342"
    ],
    "NZBN": "9429127457886",
    "Email": "office@north.kiwi",
    "Start Date": "01/11/2013",
    "Location": "Arkwrighttown"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.nnete.com",
    "Registered Address": [
      "40 Burnham Parade",
      "",
      "Montgomery",
      "3728"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "17 Simonson Road",
      "Yap",
      "Rugglesville",
      "3381"
    ],
    "Phone Number": "+64 25 6241552",
    "Legal Entity Name": "NNETE Limited",
    "Directors": [
      "Claudia Wood",
      "Virgil Hobson",
      "Rina Wembley",
      "Laureen Badcock",
      "Jamie Arkwright"
    ],
    "Company Number": "1865697",
    "Principal Place of Business": [
      "19w Strudwick Road",
      "Smythe",
      "Fryeton",
      "7748"
    ],
    "NZBN": "9429127464181",
    "Email": "sales@nnete.com",
    "Start Date": "23/03/1991",
    "Location": "Fryeton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.taskertrode.com",
    "Registered Address": [
      "99 Allard Rd",
      "Bond",
      "Bonherton",
      "7613"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "168 Marlow St",
      "Marsden",
      "Tennison City",
      "2510"
    ],
    "Phone Number": "+64 27 5635680",
    "Legal Entity Name": "Taskertrode Limited",
    "Directors": [
      "Isaiah Britton",
      "Ruben Tennyson",
      "Cecily Stephenson",
      "Alisha Morrish",
      "Allison Rowbottom",
      "Eli Meadows"
    ],
    "Company Number": "1866016",
    "Principal Place of Business": [
      "17 Ikin Rd",
      "Barber",
      "Martinston",
      "5418"
    ],
    "NZBN": "9429127467267",
    "Email": "info@taskertrode.com",
    "Start Date": "08/06/2006",
    "Location": "Martinston"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.swd.net.nz",
    "Registered Address": [
      "45/75j Hilton Rd",
      "Haywood",
      "Benbow",
      "5778"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "45/75j Hilton Rd",
      "Haywood",
      "Benbow",
      "5778"
    ],
    "Phone Number": "+64 29 5402810",
    "Legal Entity Name": "SWD Limited",
    "Directors": [
      "Christabel Howe",
      "Keith Holmwood",
      "Morgan Anderson"
    ],
    "Company Number": "1866493",
    "Principal Place of Business": [
      "45/75j Hilton Rd",
      "Haywood",
      "Benbow",
      "5778"
    ],
    "NZBN": "9429127470335",
    "Email": "admin@swd.net.nz",
    "Start Date": "24/10/1991",
    "Location": "Benbow"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.garrodsolutions.nz",
    "Registered Address": [
      "96 Coombs Avenue",
      "Brown",
      "Seabrookeville",
      "7710"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "96 Coombs Avenue",
      "Brown",
      "Seabrookeville",
      "7710"
    ],
    "Phone Number": "+64 11 6759601",
    "Legal Entity Name": "Garrod Solutions Limited",
    "Directors": [
      "Quentin Scriven",
      "Flora Cooke",
      "Thea Daniel",
      "Loreen Wembley",
      "Alexa Foss",
      "Kirk Sanders",
      "Gerald Williamson",
      "Floyd Banks"
    ],
    "Company Number": "1866586",
    "Principal Place of Business": [
      "96 Coombs Avenue",
      "Brown",
      "Seabrookeville",
      "7710"
    ],
    "NZBN": "9429127470915",
    "Email": "sales@garrodsolutions.nz",
    "Start Date": "27/04/1986",
    "Location": "Seabrookeville"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.interverityenterprises.nz",
    "Registered Address": [
      "31/1 Sanderson Avenue",
      "Gregory",
      "Sauvage",
      "4407"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Interverity Enterprises",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "12/48 Underwood Highway",
      "",
      "Lockwood City",
      "4751"
    ],
    "Phone Number": "+64 18 7804721",
    "Legal Entity Name": "Interverity Enterprises Limited",
    "Directors": [
      "Nathan Longstaff"
    ],
    "Company Number": "1867127",
    "Principal Place of Business": [
      "12/48 Underwood Highway",
      "",
      "Lockwood City",
      "4751"
    ],
    "NZBN": "9429127476337",
    "Email": "isaiah@interverityenterprises.nz",
    "Start Date": "24/12/2003",
    "Location": "Lockwood City"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.megastark.net.nz",
    "Registered Address": [
      "46 Paulson Parade",
      "Fairburn",
      "Dennelltown",
      "3721"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Megastark",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "46 Paulson Parade",
      "Fairburn",
      "Dennelltown",
      "3721"
    ],
    "Phone Number": "+64 26 7263334",
    "Legal Entity Name": "Megastark Limited",
    "Directors": [
      "Sheldon Hackett",
      "Anesa Paulson"
    ],
    "Company Number": "1867222",
    "Principal Place of Business": [
      "46 Paulson Parade",
      "Fairburn",
      "Dennelltown",
      "3721"
    ],
    "NZBN": "9429127481034",
    "Email": "info@megastark.net.nz",
    "Start Date": "25/12/1997",
    "Location": "Dennelltown"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.laa.net",
    "Registered Address": [
      "100 Williamson Rd",
      "",
      "Stoddardton",
      "7492"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "LAA",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "100 Williamson Rd",
      "",
      "Stoddardton",
      "7492"
    ],
    "Phone Number": "+64 17 8908227",
    "Legal Entity Name": "LAA Limited",
    "Directors": [
      "Kirsten Boothman"
    ],
    "Company Number": "1867682",
    "Principal Place of Business": [
      "100 Williamson Rd",
      "",
      "Stoddardton",
      "7492"
    ],
    "NZBN": "9429127484721",
    "Email": "sales@laa.net",
    "Start Date": "19/12/1999",
    "Location": "Stoddardton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.interboothmannetworks.com",
    "Registered Address": [
      "48 Neville Rd",
      "Parker",
      "Pickleton",
      "1316"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "11/30d Abrahams Street",
      "Goode",
      "Holmwoodton",
      "4790"
    ],
    "Phone Number": "+64 22 8330947",
    "Legal Entity Name": "Interboothman Networks Limited",
    "Directors": [
      "Francesca Raines",
      "Louisa Hyland",
      "Kirk Bolton",
      "Gerald Newton",
      "Celestyn Fairchild",
      "Preston Malone"
    ],
    "Company Number": "1868214",
    "Principal Place of Business": [
      "48 Neville Rd",
      "Parker",
      "Pickleton",
      "1316"
    ],
    "NZBN": "9429127492214",
    "Email": "info@interboothmannetworks.com",
    "Start Date": "19/12/1998",
    "Location": "Pickleton"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.porternetworks.net.nz",
    "Registered Address": [
      "73 Simpson Rd",
      "Cline",
      "Baxtertown",
      "6732"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "45/98 Cristianson Rd",
      "Walker",
      "Stidolphton",
      "2516"
    ],
    "Phone Number": "+64 26 6755629",
    "Legal Entity Name": "Porter Networks Limited",
    "Directors": [
      "Venessa Keegan",
      "Melanie Stainthorpe",
      "Henry Adamson",
      "Hal Bullock"
    ],
    "Company Number": "1869127",
    "Principal Place of Business": [
      "477 Hancock Street",
      "Weaver",
      "Downerville",
      "4383"
    ],
    "NZBN": "9429127496199",
    "Email": "info@porternetworks.net.nz",
    "Start Date": "10/04/1994",
    "Location": "Downerville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.derrickssaylorandyork.kiwi",
    "Registered Address": [
      "528 Womack Ave",
      "",
      "Hall",
      "2174"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "53 Thorn Rd",
      "",
      "Hobbeston",
      "3335"
    ],
    "Phone Number": "+64 24 6972001",
    "Legal Entity Name": "Derricks, Saylor and York Limited",
    "Directors": [
      "Quinn Walmsley",
      "Evan Towner",
      "Pippa Wakefield",
      "Wesley Bird",
      "Susannah Backus",
      "Hugh Brooks",
      "Christopher William",
      "Colette Rowntree"
    ],
    "Company Number": "1870066",
    "Principal Place of Business": [
      "528 Womack Ave",
      "",
      "Hall",
      "2174"
    ],
    "NZBN": "9429127499770",
    "Email": "sawyer@derrickssaylorandyork.kiwi",
    "Start Date": "30/09/2002",
    "Location": "Hall"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.simmsryeleyandjarvis.co.nz",
    "Registered Address": [
      "31/91 Suggitt St",
      "",
      "Brownlowton",
      "1098"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Film and television",
    "Address for Service": [
      "31/91 Suggitt St",
      "",
      "Brownlowton",
      "1098"
    ],
    "Phone Number": "+64 24 8798070",
    "Legal Entity Name": "Simms, Ryeley and Jarvis Limited",
    "Directors": [
      "Ian Dukeson",
      "Ryan Pocock",
      "Otto Roydon"
    ],
    "Company Number": "1870209",
    "Principal Place of Business": [
      "2/96 Eldridge Ave",
      "Rupertson",
      "Gregory",
      "5150"
    ],
    "NZBN": "9429127509318",
    "Email": "admin@simmsryeleyandjarvis.co.nz",
    "Start Date": "12/04/2009",
    "Location": "Gregory"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.megamannlands.com",
    "Registered Address": [
      "50i Thrussell Ave",
      "Sandford",
      "Yongton",
      "4638"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "82 Berry Ave",
      "",
      "Lukeson",
      "4874"
    ],
    "Phone Number": "+64 8 8744922",
    "Legal Entity Name": "Megamann Lands Limited",
    "Directors": [
      "Nikki Glover",
      "Gavin Brownlow",
      "Norman Cummins",
      "Jayme Yates",
      "Hal Royle",
      "Yves Hollins",
      "Wilfred Parker",
      "Virgil Huddleson"
    ],
    "Company Number": "1870707",
    "Principal Place of Business": [
      "43 Keen Ave",
      "Bloxam",
      "Gilbertland",
      "1153"
    ],
    "NZBN": "9429127510840",
    "Email": "admin@megamannlands.com",
    "Start Date": "27/11/1994",
    "Location": "Gilbertland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.hogarthcorp.nz",
    "Registered Address": [
      "25 Brassington Rd",
      "Honeysett",
      "Scrivenville",
      "3360"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Hogarthcorp",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "236 Strickland St",
      "Howland",
      "Sandersonton",
      "3554"
    ],
    "Phone Number": "+64 4 6595868",
    "Legal Entity Name": "Hogarthcorp Limited",
    "Directors": [
      "Donella Cross",
      "Vaughn Bird",
      "Caprice Simonson"
    ],
    "Company Number": "1871113",
    "Principal Place of Business": [
      "41 Georgeson Parade",
      "",
      "Thorne City",
      "2017"
    ],
    "NZBN": "9429127513643",
    "Email": "office@hogarthcorp.nz",
    "Start Date": "06/12/1994",
    "Location": "Thorne City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ericsontechnologies.co.nz",
    "Registered Address": [
      "90 Stenet Highway",
      "Butler",
      "Ledfordland",
      "1427"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "30/43 Simmons Highway",
      "Dixon",
      "Dalton",
      "6149"
    ],
    "Phone Number": "+64 28 6240322",
    "Legal Entity Name": "Ericson Technologies Limited",
    "Directors": [
      "Quincy Freeman",
      "Tyler Scott",
      "Valda Woodward",
      "Michelle Haden",
      "Ian Haward",
      "Conner Royce",
      "Toni Olhouser",
      "Brady Howland"
    ],
    "Company Number": "1871967",
    "Principal Place of Business": [
      "15 Hampson Ave",
      "Hope",
      "Rogersontown",
      "3887"
    ],
    "NZBN": "9429127522584",
    "Email": "info@ericsontechnologies.co.nz",
    "Start Date": "30/12/1998",
    "Location": "Rogersontown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.atc.nz",
    "Registered Address": [
      "735 Butcher St",
      "Daniel",
      "Spurlingville",
      "8335"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "36 Wheeler Rd",
      "Senior",
      "Whineryville",
      "4299"
    ],
    "Phone Number": "+64 19 6513600",
    "Legal Entity Name": "ATC Limited",
    "Directors": [
      "Isaiah Goode",
      "Kalani Moses",
      "Dylan Huff"
    ],
    "Company Number": "1872314",
    "Principal Place of Business": [
      "2 Meadows Avenue",
      "Ackerman",
      "Thornton",
      "7943"
    ],
    "NZBN": "9429127528906",
    "Email": "office@atc.nz",
    "Start Date": "30/11/1997",
    "Location": "Thornton"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.rwr.co.nz",
    "Registered Address": [
      "56n Berry Rd",
      "Tipton",
      "Fayton",
      "2651"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Myers, Spearing and Frost",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "43/35 Stainthorpe Road",
      "",
      "Jenkins",
      "4342"
    ],
    "Phone Number": "+64 6 8375392",
    "Legal Entity Name": "RWR Limited",
    "Directors": [
      "Jonathan Ramsey",
      "Merritt Reed",
      "Floyd Jeffery"
    ],
    "Company Number": "1872911",
    "Principal Place of Business": [
      "43/35 Stainthorpe Road",
      "",
      "Jenkins",
      "4342"
    ],
    "NZBN": "9429127538172",
    "Email": "office@rwr.co.nz",
    "Start Date": "04/09/1993",
    "Location": "Jenkins"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.samuels.nz",
    "Registered Address": [
      "752s Clifford Rd",
      "",
      "Southersville",
      "3847"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "34z Holmwood Ave",
      "Crawford",
      "Adamson",
      "1094"
    ],
    "Phone Number": "+64 12 6872862",
    "Legal Entity Name": "Samuels Limited",
    "Directors": [
      "Mason Milton",
      "Anthony Booner",
      "Jonathan Wheeler",
      "Marjorie Wilson",
      "Daniel Ogden",
      "Vaughn Wilkerson"
    ],
    "Company Number": "1873057",
    "Principal Place of Business": [
      "69c Victors St",
      "",
      "Traskton",
      "7848"
    ],
    "NZBN": "9429127548096",
    "Email": "office@samuels.nz",
    "Start Date": "16/05/1995",
    "Location": "Traskton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.interhoopertechnologies.kiwi",
    "Registered Address": [
      "95g Granger Avenue",
      "Younge",
      "Perryton",
      "2052"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "95g Granger Avenue",
      "Younge",
      "Perryton",
      "2052"
    ],
    "Phone Number": "+64 26 8586117",
    "Legal Entity Name": "Interhooper Technologies Limited",
    "Directors": [
      "Bernadette Morce",
      "Shelly Woodham",
      "Dora Booner",
      "Cory Goode",
      "Quinn Tyson"
    ],
    "Company Number": "1873299",
    "Principal Place of Business": [
      "93 Mathews Street",
      "",
      "Christinsen",
      "8650"
    ],
    "NZBN": "9429127549895",
    "Email": "office@interhoopertechnologies.kiwi",
    "Start Date": "09/11/2001",
    "Location": "Christinsen"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.hawardandmitchell.nz",
    "Registered Address": [
      "80 Kitchen Rd",
      "",
      "Foresterton",
      "7890"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Avery",
    "Business Industry Classification": "Film and television",
    "Address for Service": [
      "8 Hollins Ave",
      "Fairburn",
      "Fletcher",
      "2558"
    ],
    "Phone Number": "+64 25 5815506",
    "Legal Entity Name": "Haward and Mitchell Limited",
    "Directors": [
      "Tessi Janson",
      "Joyce Abrahams",
      "Jason Revie",
      "Christian Hodson",
      "Kim Bunker",
      "Jason Sempers",
      "Jasmine Vaughn"
    ],
    "Company Number": "1874222",
    "Principal Place of Business": [
      "584 Washington Parade",
      "",
      "Baldwinville",
      "3546"
    ],
    "NZBN": "9429127554905",
    "Email": "office@hawardandmitchell.nz",
    "Start Date": "21/08/1995",
    "Location": "Baldwinville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.megachristinsencorp.net.nz",
    "Registered Address": [
      "351 Jeffery Avenue",
      "Steffen",
      "Patterson",
      "1160"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "PORP",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "351 Jeffery Avenue",
      "Steffen",
      "Patterson",
      "1160"
    ],
    "Phone Number": "+64 8 8132163",
    "Legal Entity Name": "Megachristinsencorp Limited",
    "Directors": [
      "Charmain Willis",
      "Wilma Parent",
      "Zachary Morrison",
      "Marilyn Southers",
      "Kathe Quick",
      "James Holmes"
    ],
    "Company Number": "1874822",
    "Principal Place of Business": [
      "351 Jeffery Avenue",
      "Steffen",
      "Patterson",
      "1160"
    ],
    "NZBN": "9429127556718",
    "Email": "admin@megachristinsencorp.net.nz",
    "Start Date": "17/07/2000",
    "Location": "Patterson"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.chancescope.nz",
    "Registered Address": [
      "30 Yates Parade",
      "Stephenson",
      "Atterberryton",
      "6489"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "551 Snider Street",
      "",
      "Wragge City",
      "5797"
    ],
    "Phone Number": "+64 26 8832398",
    "Legal Entity Name": "Chancescope Limited",
    "Directors": [
      "Quincy Nathans",
      "Madeline House",
      "Elissa Hampson",
      "Candy Wheeler",
      "Kevin Hardy"
    ],
    "Company Number": "1875135",
    "Principal Place of Business": [
      "100 Dixon St",
      "Benbow",
      "Stidolphville",
      "3927"
    ],
    "NZBN": "9429127565130",
    "Email": "office@chancescope.nz",
    "Start Date": "09/05/2010",
    "Location": "Stidolphville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.huddlesontech.nz",
    "Registered Address": [
      "92 Owston Ave",
      "Tyson",
      "Richardton",
      "3472"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "92 Owston Ave",
      "Tyson",
      "Richardton",
      "3472"
    ],
    "Phone Number": "+64 27 7282366",
    "Legal Entity Name": "Huddlesontech Limited",
    "Directors": [
      "Dahlia Woods",
      "Janessa Sempers"
    ],
    "Company Number": "1875639",
    "Principal Place of Business": [
      "92 Owston Ave",
      "Tyson",
      "Richardton",
      "3472"
    ],
    "NZBN": "9429127572985",
    "Email": "info@huddlesontech.nz",
    "Start Date": "18/11/2009",
    "Location": "Richardton"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.nnt.com",
    "Registered Address": [
      "36z Victor St",
      "Simonson",
      "Myers",
      "7782"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Cox Technologies",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "36z Victor St",
      "Simonson",
      "Myers",
      "7782"
    ],
    "Phone Number": "+64 21 5272487",
    "Legal Entity Name": "NNT Limited",
    "Directors": [
      "Trudie Samson",
      "Patrick Ellsworth",
      "Riley Hanley",
      "Anthony Purcell"
    ],
    "Company Number": "1875786",
    "Principal Place of Business": [
      "69 Street Road",
      "Bonney",
      "Ethansonton",
      "1419"
    ],
    "NZBN": "9429127576570",
    "Email": "sales@nnt.com",
    "Start Date": "28/02/2003",
    "Location": "Ethansonton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.cooksonlands.co.nz",
    "Registered Address": [
      "93 Burton Street",
      "",
      "Nathansonton",
      "6801"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "72g Kimball Avenue",
      "",
      "Tailorville",
      "4242"
    ],
    "Phone Number": "+64 26 7370973",
    "Legal Entity Name": "Cookson Lands Limited",
    "Directors": [
      "Hans Ryley",
      "Alexander Dickens",
      "Keith Readdie",
      "Timothy Harman"
    ],
    "Company Number": "1876385",
    "Principal Place of Business": [
      "78 Myles Avenue",
      "Treloar",
      "Slaterton",
      "6334"
    ],
    "NZBN": "9429127579847",
    "Email": "bryson@cooksonlands.co.nz",
    "Start Date": "26/02/2013",
    "Location": "Slaterton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.brb.nz",
    "Registered Address": [
      "71 Underhill Rd",
      "",
      "Taskerland",
      "6314"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Ansontrode",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "29 School St",
      "Blythe",
      "Hill City",
      "3604"
    ],
    "Phone Number": "+64 18 5321981",
    "Legal Entity Name": "BRB Limited",
    "Directors": [
      "Norman Sudworth",
      "Scott Sniders",
      "Nigel Scrivener",
      "Valaree Fenn",
      "Gordon Whinery",
      "Robert Bristol",
      "Wilson Stevenson",
      "Jonathan Auteberry"
    ],
    "Company Number": "1877352",
    "Principal Place of Business": [
      "5 Kirby Parade",
      "Morse",
      "Hobbston",
      "6188"
    ],
    "NZBN": "9429127581291",
    "Email": "janet@brb.nz",
    "Start Date": "14/09/2000",
    "Location": "Hobbston"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.hrrhc.kiwi",
    "Registered Address": [
      "91 York Ave",
      "",
      "Mathewsontown",
      "3169"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "49b Keen Street",
      "",
      "Montgomerytown",
      "2981"
    ],
    "Phone Number": "+64 4 6477419",
    "Legal Entity Name": "HRRHC Limited",
    "Directors": [
      "Harriet Timothyson",
      "Flynn Owston"
    ],
    "Company Number": "1877442",
    "Principal Place of Business": [
      "91 York Ave",
      "",
      "Mathewsontown",
      "3169"
    ],
    "NZBN": "9429127590552",
    "Email": "info@hrrhc.kiwi",
    "Start Date": "22/10/2002",
    "Location": "Mathewsontown"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.horneandspear.net.nz",
    "Registered Address": [
      "97p Matthewson Road",
      "Boone",
      "Bonneytown",
      "1674"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "97p Matthewson Road",
      "Boone",
      "Bonneytown",
      "1674"
    ],
    "Phone Number": "+64 4 7187997",
    "Legal Entity Name": "Horne and Spear Limited",
    "Directors": [
      "Trudy Danniell"
    ],
    "Company Number": "1877670",
    "Principal Place of Business": [
      "98 Benbow St",
      "",
      "Rowbottomton",
      "4546"
    ],
    "NZBN": "9429127594369",
    "Email": "office@horneandspear.net.nz",
    "Start Date": "17/07/2015",
    "Location": "Rowbottomton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.interrichard.net",
    "Registered Address": [
      "74 Padmore Ave",
      "",
      "Botwrightton",
      "8298"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Interrichard",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "74 Padmore Ave",
      "",
      "Botwrightton",
      "8298"
    ],
    "Phone Number": "+64 15 7685329",
    "Legal Entity Name": "Interrichard Limited",
    "Directors": [
      "Bessie Blackburn",
      "Donna Hancock",
      "Romana Rennoll",
      "Jeana Taylor"
    ],
    "Company Number": "1878156",
    "Principal Place of Business": [
      "52 Causey Parade",
      "",
      "Thorn City",
      "5971"
    ],
    "NZBN": "9429127597377",
    "Email": "info@interrichard.net",
    "Start Date": "26/11/1999",
    "Location": "Thorn City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.clcix.net",
    "Registered Address": [
      "695 Scrivenor St",
      "",
      "Jarvis City",
      "5813"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "23 Keegan Street",
      "Pryor",
      "Bulletown",
      "2836"
    ],
    "Phone Number": "+64 10 8887513",
    "Legal Entity Name": "CLCIX Limited",
    "Directors": [
      "Caprice Huff",
      "Iris Rogers"
    ],
    "Company Number": "1878656",
    "Principal Place of Business": [
      "695 Scrivenor St",
      "",
      "Jarvis City",
      "5813"
    ],
    "NZBN": "9429127601944",
    "Email": "office@clcix.net",
    "Start Date": "30/07/1990",
    "Location": "Jarvis City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ese.nz",
    "Registered Address": [
      "31/565 Matthewson Highway",
      "Winfield",
      "Daviesville",
      "6504"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "ESE",
    "Business Industry Classification": "Film and television",
    "Address for Service": [
      "3 Ayton Highway",
      "Carlisle",
      "Thwaite City",
      "3175"
    ],
    "Phone Number": "+64 5 7466123",
    "Legal Entity Name": "ESE Limited",
    "Directors": [
      "Carlee Rowbottom",
      "Milton Darby",
      "Ruben Ford"
    ],
    "Company Number": "1879126",
    "Principal Place of Business": [
      "355 Garner Street",
      "Boyce",
      "Bonnertown",
      "8324"
    ],
    "NZBN": "9429127607649",
    "Email": "admin@ese.nz",
    "Start Date": "07/02/2006",
    "Location": "Bonnertown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.rollins.net.nz",
    "Registered Address": [
      "18 Hobbs St",
      "",
      "Hunnisettton",
      "4988"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "16 Tailor Street",
      "Derricks",
      "Blackbourne",
      "2356"
    ],
    "Phone Number": "+64 3 8873684",
    "Legal Entity Name": "Rollins Limited",
    "Directors": [
      "Sheldon Jamison",
      "Dorothy Haggard",
      "Orson I'Anson",
      "Emma Miller"
    ],
    "Company Number": "1879536",
    "Principal Place of Business": [
      "73 Matthews Road",
      "Hume",
      "Brewsterland",
      "3636"
    ],
    "NZBN": "9429127616894",
    "Email": "sales@rollins.net.nz",
    "Start Date": "01/12/1998",
    "Location": "Brewsterland"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.jamisonandtriggs.nz",
    "Registered Address": [
      "866d Danniel Parade",
      "Haywood",
      "Millertown",
      "8430"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "866d Danniel Parade",
      "Haywood",
      "Millertown",
      "8430"
    ],
    "Phone Number": "+64 12 8729238",
    "Legal Entity Name": "Jamison and Triggs Limited",
    "Directors": [
      "Roy Howland",
      "Margie Harris",
      "Blossom Morison",
      "Hilda Daniels",
      "Ryan Kipling",
      "Owen Royle",
      "Valeria Bonner",
      "Philip Ingram"
    ],
    "Company Number": "1880458",
    "Principal Place of Business": [
      "86l Roscoe Highway",
      "Bardsley",
      "Earlsonland",
      "2100"
    ],
    "NZBN": "9429127624325",
    "Email": "office@jamisonandtriggs.nz",
    "Start Date": "22/03/1996",
    "Location": "Earlsonland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.kimballscope.co.nz",
    "Registered Address": [
      "68n Simonson Highway",
      "Spalding",
      "Lomantown",
      "2290"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "29b Warren Road",
      "Marston",
      "Alexanderton",
      "3675"
    ],
    "Phone Number": "+64 17 6768377",
    "Legal Entity Name": "Kimballscope Limited",
    "Directors": [
      "Christal Timothyson",
      "Philip Polley",
      "Karena Thomas",
      "Foster Gore",
      "Salena Elliot",
      "Belinda Towner",
      "Wilbur Stack"
    ],
    "Company Number": "1880871",
    "Principal Place of Business": [
      "91 Thomas Rd",
      "",
      "Rakeston",
      "5221"
    ],
    "NZBN": "9429127629559",
    "Email": "sales@kimballscope.co.nz",
    "Start Date": "25/04/1997",
    "Location": "Rakeston"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.blackburntrode.co.nz",
    "Registered Address": [
      "32/29 Banister Avenue",
      "Firmin",
      "Braddockville",
      "7782"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "23/69 Baxter Street",
      "Deering",
      "Hallmanville",
      "6654"
    ],
    "Phone Number": "+64 7 7932737",
    "Legal Entity Name": "Blackburntrode Limited",
    "Directors": [
      "Gabriela Huff",
      "Fifi Killam"
    ],
    "Company Number": "1881508",
    "Principal Place of Business": [
      "32/29 Banister Avenue",
      "Firmin",
      "Braddockville",
      "7782"
    ],
    "NZBN": "9429127639183",
    "Email": "office@blackburntrode.co.nz",
    "Start Date": "31/07/1995",
    "Location": "Braddockville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.brassingtontrode.net",
    "Registered Address": [
      "38 Peters Rd",
      "Hogarth",
      "Treviston",
      "1419"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Brassingtontrode",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "22 Day Rd",
      "Rennoll",
      "Battle City",
      "1318"
    ],
    "Phone Number": "+64 5 6528873",
    "Legal Entity Name": "Brassingtontrode Limited",
    "Directors": [
      "Sandy Rigby",
      "Olin Travers",
      "Brady Midgley",
      "Monique Brassington",
      "Sean Rennold",
      "Matthew Archer",
      "Alma Bonher",
      "Ferris Arnold"
    ],
    "Company Number": "1881624",
    "Principal Place of Business": [
      "97 Hunter Rd",
      "Deadman",
      "Philipsland",
      "2727"
    ],
    "NZBN": "9429127647423",
    "Email": "sales@brassingtontrode.net",
    "Start Date": "07/08/1990",
    "Location": "Philipsland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.sno.co.nz",
    "Registered Address": [
      "14 Arrington Rd",
      "",
      "Quickton",
      "1596"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "SNO",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "26 Vipond Ave",
      "Sumner",
      "Bloxamton",
      "3232"
    ],
    "Phone Number": "+64 21 6602650",
    "Legal Entity Name": "SNO Limited",
    "Directors": [
      "Logan Cummins",
      "Ambrosia Harris",
      "Kylie Travis",
      "Delyth Hawkins"
    ],
    "Company Number": "1882052",
    "Principal Place of Business": [
      "47 Henderson Parade",
      "Gibbs",
      "Hopkinsland",
      "5823"
    ],
    "NZBN": "9429127655633",
    "Email": "sales@sno.co.nz",
    "Start Date": "29/06/2011",
    "Location": "Hopkinsland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.brown.co.nz",
    "Registered Address": [
      "45 Reynolds Ave",
      "Travis",
      "Cooketon",
      "1212"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "21/73 Vance Rd",
      "Evered",
      "Roundsville",
      "1815"
    ],
    "Phone Number": "+64 25 6450471",
    "Legal Entity Name": "Brown Limited",
    "Directors": [
      "Omar Loman",
      "Zebulon Hopkins",
      "Isaiah Ethans",
      "Flynn Coburn",
      "Roland Haggard"
    ],
    "Company Number": "1882575",
    "Principal Place of Business": [
      "763 Rakes Street",
      "",
      "Crewe City",
      "5998"
    ],
    "NZBN": "9429127658047",
    "Email": "office@brown.co.nz",
    "Start Date": "21/11/2007",
    "Location": "Crewe City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.fennexperience.nz",
    "Registered Address": [
      "97 Danniell Ave",
      "Marston",
      "Holmwoodton",
      "1813"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "87 Fairchild Parade",
      "",
      "Myerston",
      "6580"
    ],
    "Phone Number": "+64 13 7990803",
    "Legal Entity Name": "Fenn Experience Limited",
    "Directors": [
      "Foster Dickenson"
    ],
    "Company Number": "1883318",
    "Principal Place of Business": [
      "57 Hume Avenue",
      "Blythe",
      "Hendryton",
      "6344"
    ],
    "NZBN": "9429127658245",
    "Email": "sales@fennexperience.nz",
    "Start Date": "30/03/2009",
    "Location": "Hendryton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.badcocklands.kiwi",
    "Registered Address": [
      "13g Skinner Road",
      "",
      "Hawardville",
      "5796"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Badcock Lands",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "89 Travers Rd",
      "Sexton",
      "Watkinsland",
      "4913"
    ],
    "Phone Number": "+64 7 8160563",
    "Legal Entity Name": "Badcock Lands Limited",
    "Directors": [
      "Hans Eldridge",
      "Yancy Downer",
      "Ruben Weekes",
      "Jacinta Haywood",
      "Rochelle Stenet",
      "Trevor Woodward"
    ],
    "Company Number": "1883556",
    "Principal Place of Business": [
      "37/16 Verity Street",
      "Close",
      "Sumnerland",
      "5024"
    ],
    "NZBN": "9429127662464",
    "Email": "admin@badcocklands.kiwi",
    "Start Date": "23/09/2006",
    "Location": "Sumnerland"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.megarainesexperience.net.nz",
    "Registered Address": [
      "36 Glendon Avenue",
      "",
      "Brayton",
      "6038"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Megaraines Experience",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "78 Stanton Ave",
      "",
      "Constableton",
      "3796"
    ],
    "Phone Number": "+64 4 7389161",
    "Legal Entity Name": "Megaraines Experience Limited",
    "Directors": [
      "Isaac Ethanson",
      "Kristal Lawson",
      "Jason Timothyson",
      "Keith Beattie",
      "Sean Skinner"
    ],
    "Company Number": "1884073",
    "Principal Place of Business": [
      "78 Stanton Ave",
      "",
      "Constableton",
      "3796"
    ],
    "NZBN": "9429127663737",
    "Email": "office@megarainesexperience.net.nz",
    "Start Date": "23/03/2004",
    "Location": "Constableton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.megachurchlands.co.nz",
    "Registered Address": [
      "69o Cristiansen St",
      "Pierson",
      "Bishopville",
      "1013"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Megachurch Lands",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "75 Queshire St",
      "Franklin",
      "Emerson",
      "7998"
    ],
    "Phone Number": "+64 19 5268692",
    "Legal Entity Name": "Megachurch Lands Limited",
    "Directors": [
      "Veronica Christianson",
      "Helena Daniels",
      "Madeline Bancroft",
      "Aaron Malone",
      "Kevin Hathoway",
      "Kalee Barker",
      "Newman Woodhams",
      "Ignatius Henderson"
    ],
    "Company Number": "1884191",
    "Principal Place of Business": [
      "75 Queshire St",
      "Franklin",
      "Emerson",
      "7998"
    ],
    "NZBN": "9429127666806",
    "Email": "vernon@megachurchlands.co.nz",
    "Start Date": "04/01/2002",
    "Location": "Emerson"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.woods.nz",
    "Registered Address": [
      "81s Linwood Rd",
      "Haywood",
      "Vaughnville",
      "5044"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Battle, Cannon and Rounds",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "99 Skinner Road",
      "Hart",
      "Hancockton",
      "6385"
    ],
    "Phone Number": "+64 17 8953418",
    "Legal Entity Name": "Woods Limited",
    "Directors": [
      "Anthony Orman"
    ],
    "Company Number": "1884812",
    "Principal Place of Business": [
      "89 Saylor Rd",
      "Tobias",
      "Thorne",
      "2777"
    ],
    "NZBN": "9429127668923",
    "Email": "sales@woods.nz",
    "Start Date": "12/09/1997",
    "Location": "Thorne"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.megatiptontech.net.nz",
    "Registered Address": [
      "4 Hendry Avenue",
      "Bonham",
      "Polleyton",
      "4499"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Construction",
    "Address for Service": [
      "33/75 Wolfe St",
      "",
      "Ibbottton",
      "3331"
    ],
    "Phone Number": "+64 29 6670286",
    "Legal Entity Name": "Megatiptontech Limited",
    "Directors": [
      "Frederick Holmwood",
      "Kira Thomas",
      "Hayden Hext",
      "Zachary Whittemore",
      "Arlene Hollins",
      "Gabriella Madison",
      "Quinn Babcock"
    ],
    "Company Number": "1885518",
    "Principal Place of Business": [
      "7 Allsopp Ave",
      "Atkins",
      "Thorpe",
      "4745"
    ],
    "NZBN": "9429127670285",
    "Email": "info@megatiptontech.net.nz",
    "Start Date": "09/03/1990",
    "Location": "Thorpe"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ete.co.nz",
    "Registered Address": [
      "35 Hillam Rd",
      "Tupper",
      "Woodton",
      "1445"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Simpkinscape",
    "Business Industry Classification": "Film and television",
    "Address for Service": [
      "872 Prescott Road",
      "Dick",
      "Hathewayville",
      "5094"
    ],
    "Phone Number": "+64 4 5683993",
    "Legal Entity Name": "ETE Limited",
    "Directors": [
      "Maude Robbins",
      "Rhianna Nielson",
      "Brittany Bryson",
      "Oswald Hobson",
      "Savina Myles",
      "Leisha Belcher",
      "Kirsten Wallace",
      "Chelsea Blackbourne"
    ],
    "Company Number": "1886066",
    "Principal Place of Business": [
      "872 Prescott Road",
      "Dick",
      "Hathewayville",
      "5094"
    ],
    "NZBN": "9429127673477",
    "Email": "office@ete.co.nz",
    "Start Date": "20/01/1997",
    "Location": "Hathewayville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.masth.nz",
    "Registered Address": [
      "887 Whinery St",
      "Tatham",
      "Morseland",
      "1347"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Energy",
    "Address for Service": [
      "887 Whinery St",
      "Tatham",
      "Morseland",
      "1347"
    ],
    "Phone Number": "+64 19 6076921",
    "Legal Entity Name": "MASTH Limited",
    "Directors": [
      "Lydia Vernon",
      "Olivia Cooper",
      "Eli Clausson",
      "Gertrud Thorne",
      "Zebulon Ryely"
    ],
    "Company Number": "1886647",
    "Principal Place of Business": [
      "76 Jeffery Ave",
      "Salvage",
      "Boyce",
      "4344"
    ],
    "NZBN": "9429127677833",
    "Email": "info@masth.nz",
    "Start Date": "12/05/2003",
    "Location": "Boyce"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.spse.com",
    "Registered Address": [
      "17 Elmerson St",
      "Byrd",
      "Stampton",
      "5793"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "SPSE",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "17 Elmerson St",
      "Byrd",
      "Stampton",
      "5793"
    ],
    "Phone Number": "+64 17 7587450",
    "Legal Entity Name": "SPSE Limited",
    "Directors": [
      "Wesley Thacker",
      "William Fairchild",
      "Catalina Travis"
    ],
    "Company Number": "1887540",
    "Principal Place of Business": [
      "17 Elmerson St",
      "Byrd",
      "Stampton",
      "5793"
    ],
    "NZBN": "9429127681656",
    "Email": "sales@spse.com",
    "Start Date": "17/12/1998",
    "Location": "Stampton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.carternetworks.co.nz",
    "Registered Address": [
      "709 Croft Road",
      "Fitzroy",
      "Ansonville",
      "7536"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "VNI",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "80z Cropper Ave",
      "Christopher",
      "Nevilleton",
      "5094"
    ],
    "Phone Number": "+64 21 8764886",
    "Legal Entity Name": "Carter Networks Limited",
    "Directors": [
      "Troy Pierson",
      "Ian Royle",
      "Dolores Josephs",
      "Carolina Woods",
      "Troy Benbow",
      "Zane Horne",
      "Kylie Warren"
    ],
    "Company Number": "1888272",
    "Principal Place of Business": [
      "709 Croft Road",
      "Fitzroy",
      "Ansonville",
      "7536"
    ],
    "NZBN": "9429127691075",
    "Email": "suzy@carternetworks.co.nz",
    "Start Date": "02/05/1988",
    "Location": "Ansonville"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ryra.kiwi",
    "Registered Address": [
      "40 Thomas Avenue",
      "Bloxam",
      "Akersland",
      "2719"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "RYRA",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "68 Ryeley Avenue",
      "",
      "Cheshiretown",
      "7518"
    ],
    "Phone Number": "+64 25 6423384",
    "Legal Entity Name": "RYRA Limited",
    "Directors": [
      "Dina Bryson",
      "Henry Easom",
      "Edward Reynolds",
      "Evan Purcell",
      "Nicholas Jinks",
      "Nathan Bunker",
      "Otto Beckham"
    ],
    "Company Number": "1889133",
    "Principal Place of Business": [
      "247 Reed Ave",
      "Emerson",
      "Saylorland",
      "1070"
    ],
    "NZBN": "9429127697886",
    "Email": "info@ryra.kiwi",
    "Start Date": "27/07/2014",
    "Location": "Saylorland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.dukesonandcorra.nz",
    "Registered Address": [
      "8/25 Honeycutt Street",
      "Bennet",
      "Earl City",
      "2871"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Dukeson and Corra",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "8/25 Honeycutt Street",
      "Bennet",
      "Earl City",
      "2871"
    ],
    "Phone Number": "+64 8 6335740",
    "Legal Entity Name": "Dukeson and Corra Limited",
    "Directors": [
      "Isaiah Hodson",
      "Jessi Hardwick",
      "Paula Davison",
      "Gregory Evanson",
      "Helaine Fairbairn",
      "Patricia Raines"
    ],
    "Company Number": "1889407",
    "Principal Place of Business": [
      "9/78 Wragge Parade",
      "Tinker",
      "Midgleyton",
      "6903"
    ],
    "NZBN": "9429127703211",
    "Email": "info@dukesonandcorra.nz",
    "Start Date": "15/06/1993",
    "Location": "Midgleyton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.nnato.kiwi",
    "Registered Address": [
      "55 Swindlehurst Avenue",
      "",
      "Derricksville",
      "3147"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Brown",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "2 Batts St",
      "",
      "Foxland",
      "4971"
    ],
    "Phone Number": "+64 6 8910516",
    "Legal Entity Name": "NNATO Limited",
    "Directors": [
      "Kurt Bramson",
      "Christabel Fay",
      "Morgan Dwerryhouse",
      "Rosalind Wyght"
    ],
    "Company Number": "1890311",
    "Principal Place of Business": [
      "2 Batts St",
      "",
      "Foxland",
      "4971"
    ],
    "NZBN": "9429127710608",
    "Email": "info@nnato.kiwi",
    "Start Date": "10/05/1998",
    "Location": "Foxland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.interweaver.net.nz",
    "Registered Address": [
      "69 Ecclestone St",
      "Victor",
      "Millsville",
      "3713"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Mills, Gardenar and Watkins",
    "Business Industry Classification": "Wholesale trade",
    "Address for Service": [
      "69 Ecclestone St",
      "Victor",
      "Millsville",
      "3713"
    ],
    "Phone Number": "+64 26 5240699",
    "Legal Entity Name": "Interweaver Limited",
    "Directors": [
      "Miranda Herberts",
      "Kimberley Sawyer",
      "Dean Brewster",
      "Kristina Prescott"
    ],
    "Company Number": "1890342",
    "Principal Place of Business": [
      "35/59s Davis Ave",
      "Myles",
      "Jenkins",
      "6884"
    ],
    "NZBN": "9429127716037",
    "Email": "admin@interweaver.net.nz",
    "Start Date": "13/10/2015",
    "Location": "Jenkins"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.interunderwoodtechnologies.co.nz",
    "Registered Address": [
      "67 Peterson Highway",
      "",
      "Jefferston",
      "8733"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Interunderwood Technologies",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "39 Earlson St",
      "Thorn",
      "Boonerville",
      "2813"
    ],
    "Phone Number": "+64 20 8831849",
    "Legal Entity Name": "Interunderwood Technologies Limited",
    "Directors": [
      "Frederick Arnold",
      "Harold Tinker"
    ],
    "Company Number": "1890390",
    "Principal Place of Business": [
      "67 Peterson Highway",
      "",
      "Jefferston",
      "8733"
    ],
    "NZBN": "9429127716600",
    "Email": "sales@interunderwoodtechnologies.co.nz",
    "Start Date": "14/10/2005",
    "Location": "Jefferston"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.bre.com",
    "Registered Address": [
      "23 Triggs Parade",
      "Breckinridge",
      "Hullville",
      "7910"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "23 Triggs Parade",
      "Breckinridge",
      "Hullville",
      "7910"
    ],
    "Phone Number": "+64 21 8628668",
    "Legal Entity Name": "BRE Limited",
    "Directors": [
      "Hugh Peak",
      "Irena Christisen",
      "Yuri Elliot",
      "James Stringer",
      "Sander Edison",
      "Gail Blake",
      "Tami Linwood",
      "Mason Dickenson"
    ],
    "Company Number": "1890982",
    "Principal Place of Business": [
      "74 Babcoke Ave",
      "Peterson",
      "Banisterland",
      "2815"
    ],
    "NZBN": "9429127724483",
    "Email": "office@bre.com",
    "Start Date": "02/05/1993",
    "Location": "Banisterland"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.staffordnetworks.co.nz",
    "Registered Address": [
      "20 Mallory Street",
      "",
      "Fairbairntown",
      "1491"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Stafford Networks",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "5/49 Toller Ave",
      "",
      "Huddlestonville",
      "4357"
    ],
    "Phone Number": "+64 7 8823255",
    "Legal Entity Name": "Stafford Networks Limited",
    "Directors": [
      "Karla Ikin"
    ],
    "Company Number": "1890994",
    "Principal Place of Business": [
      "25 Fear Ave",
      "",
      "Baldwinland",
      "2817"
    ],
    "NZBN": "9429127725688",
    "Email": "sean@staffordnetworks.co.nz",
    "Start Date": "13/08/2000",
    "Location": "Baldwinland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.hepburnhicksandmathers.co.nz",
    "Registered Address": [
      "37 Garrard Ave",
      "Kellogg",
      "Kitchentown",
      "8270"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "37 Garrard Ave",
      "Kellogg",
      "Kitchentown",
      "8270"
    ],
    "Phone Number": "+64 11 8100014",
    "Legal Entity Name": "Hepburn, Hicks and Mathers Limited",
    "Directors": [
      "Kyle Little",
      "Talia Parsons",
      "Dylan Daniell",
      "Hope Huxtable",
      "Thomas Murgatroyd",
      "Leigh Northrop",
      "Wade Tolbert"
    ],
    "Company Number": "1891304",
    "Principal Place of Business": [
      "33 Southers St",
      "Thatcher",
      "Peacockton",
      "4372"
    ],
    "NZBN": "9429127731443",
    "Email": "christelle@hepburnhicksandmathers.co.nz",
    "Start Date": "23/10/2002",
    "Location": "Peacockton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.longstaffexperience.nz",
    "Registered Address": [
      "14 Simpson Ave",
      "Hobbs",
      "Skinnertown",
      "8319"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Blacksoftware",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "76r Ethans Ave",
      "Lukeson",
      "Ness City",
      "3480"
    ],
    "Phone Number": "+64 5 7797429",
    "Legal Entity Name": "Longstaff Experience Limited",
    "Directors": [
      "Chantal Simpson",
      "Suzie Earl",
      "Amber Andrewson",
      "Xander Harmon",
      "Carley Rennold",
      "Trevor Backus",
      "Kristina Spurling",
      "Cecily Brassington"
    ],
    "Company Number": "1892193",
    "Principal Place of Business": [
      "14 Simpson Ave",
      "Hobbs",
      "Skinnertown",
      "8319"
    ],
    "NZBN": "9429127731627",
    "Email": "admin@longstaffexperience.nz",
    "Start Date": "26/04/2014",
    "Location": "Skinnertown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ayw.co.nz",
    "Registered Address": [
      "36/73 Gully Highway",
      "Hampson",
      "Treloarton",
      "5419"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "MAL",
    "Business Industry Classification": "Bioscience and biotechnology",
    "Address for Service": [
      "36/73 Gully Highway",
      "Hampson",
      "Treloarton",
      "5419"
    ],
    "Phone Number": "+64 29 5939916",
    "Legal Entity Name": "AYW Limited",
    "Directors": [
      "Henrietta Haywood",
      "Maud Rollins",
      "Lilly Byrd",
      "Farrah Frost",
      "Kelsey School",
      "Spencer Roberts",
      "Abegaila Blakeslee"
    ],
    "Company Number": "1892517",
    "Principal Place of Business": [
      "36/73 Gully Highway",
      "Hampson",
      "Treloarton",
      "5419"
    ],
    "NZBN": "9429127738183",
    "Email": "info@ayw.co.nz",
    "Start Date": "20/09/2001",
    "Location": "Treloarton"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.snidersandgrey.net",
    "Registered Address": [
      "8 Taylor Avenue",
      "",
      "Spence City",
      "3724"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "EWRR",
    "Business Industry Classification": "Film and television",
    "Address for Service": [
      "98 Scrivenor Avenue",
      "",
      "Chancellor",
      "8555"
    ],
    "Phone Number": "+64 7 6258599",
    "Legal Entity Name": "Sniders and Grey Limited",
    "Directors": [
      "Orson Denman",
      "Carmel Reier",
      "Molly Close",
      "Mason Merrick",
      "William Cantrell",
      "Zachary Washington"
    ],
    "Company Number": "1893292",
    "Principal Place of Business": [
      "37/46 Pressley St",
      "Witherspoon",
      "Brewsterland",
      "1432"
    ],
    "NZBN": "9429127738336",
    "Email": "sales@snidersandgrey.net",
    "Start Date": "03/12/2005",
    "Location": "Brewsterland"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.quiggbardsleyandboivin.net",
    "Registered Address": [
      "32b Stidolph Ave",
      "Tipton",
      "Barnesland",
      "3790"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Quigg, Bardsley and Boivin",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "32b Stidolph Ave",
      "Tipton",
      "Barnesland",
      "3790"
    ],
    "Phone Number": "+64 26 8496493",
    "Legal Entity Name": "Quigg, Bardsley and Boivin Limited",
    "Directors": [
      "Brian Clinton",
      "Val Butts",
      "Xander Revie",
      "Robert Ryers",
      "Elli Goffe",
      "Daniel Gardenar",
      "Harold Womack",
      "Fae Styles"
    ],
    "Company Number": "1893483",
    "Principal Place of Business": [
      "32b Stidolph Ave",
      "Tipton",
      "Barnesland",
      "3790"
    ],
    "NZBN": "9429127738831",
    "Email": "daniel@quiggbardsleyandboivin.net",
    "Start Date": "27/12/2015",
    "Location": "Barnesland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.gullyandrennell.nz",
    "Registered Address": [
      "3 Roach St",
      "Aiken",
      "Arthurton",
      "4572"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Gully and Rennell",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "38/72 Draper Ave",
      "",
      "Joinertown",
      "7321"
    ],
    "Phone Number": "+64 7 6057156",
    "Legal Entity Name": "Gully and Rennell Limited",
    "Directors": [
      "Ignatius Brooks",
      "Brandon Bancroft",
      "Eveleen Edison"
    ],
    "Company Number": "1893760",
    "Principal Place of Business": [
      "98 Jernigan St",
      "",
      "Petersontown",
      "6570"
    ],
    "NZBN": "9429127746843",
    "Email": "orson@gullyandrennell.nz",
    "Start Date": "07/03/1986",
    "Location": "Petersontown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.megaclinton.nz",
    "Registered Address": [
      "21/60 Battle St",
      "Lynn",
      "Armisteadton",
      "3891"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "21/60 Battle St",
      "Lynn",
      "Armisteadton",
      "3891"
    ],
    "Phone Number": "+64 20 7972025",
    "Legal Entity Name": "Megaclinton Limited",
    "Directors": [
      "Christian Pelley",
      "Brian Barlow",
      "Janis Simon"
    ],
    "Company Number": "1894248",
    "Principal Place of Business": [
      "21/60 Battle St",
      "Lynn",
      "Armisteadton",
      "3891"
    ],
    "NZBN": "9429127748946",
    "Email": "austin@megaclinton.nz",
    "Start Date": "12/12/1988",
    "Location": "Armisteadton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ume.net",
    "Registered Address": [
      "89v Blake Avenue",
      "Firmin",
      "Huddlesonville",
      "2965"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Allsopp and Chase",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "59 Trevis Ave",
      "",
      "Fisherton",
      "3560"
    ],
    "Phone Number": "+64 26 6728293",
    "Legal Entity Name": "UME Limited",
    "Directors": [
      "Theodore Morse",
      "Grant Bellamy",
      "Stacie Morrison"
    ],
    "Company Number": "1894302",
    "Principal Place of Business": [
      "59 Trevis Ave",
      "",
      "Fisherton",
      "3560"
    ],
    "NZBN": "9429127757986",
    "Email": "office@ume.net",
    "Start Date": "14/02/1986",
    "Location": "Fisherton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.mnb.net.nz",
    "Registered Address": [
      "35 Thwaite Parade",
      "",
      "Savage City",
      "6836"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "LEL",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "31 Barker Rd",
      "",
      "Robbinsville",
      "6231"
    ],
    "Phone Number": "+64 21 8318856",
    "Legal Entity Name": "MNB Limited",
    "Directors": [
      "Anastasia Steffen",
      "Theresa Danielson",
      "Christian Brassington",
      "Wilfred Southers",
      "Xander Hamilton",
      "Quentin Josephson",
      "Richard Hobbes",
      "Victor Butcher"
    ],
    "Company Number": "1894635",
    "Principal Place of Business": [
      "35 Thwaite Parade",
      "",
      "Savage City",
      "6836"
    ],
    "NZBN": "9429127766674",
    "Email": "bessie@mnb.net.nz",
    "Start Date": "12/04/2014",
    "Location": "Savage City"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.interoakleyscape.co.nz",
    "Registered Address": [
      "71n Davies Highway",
      "",
      "Endicottton",
      "5805"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Manufacturing and production",
    "Address for Service": [
      "71n Davies Highway",
      "",
      "Endicottton",
      "5805"
    ],
    "Phone Number": "+64 8 5594858",
    "Legal Entity Name": "Interoakleyscape Limited",
    "Directors": [
      "Beatrice Hodges",
      "Ishmael Toft",
      "Charlotte Victor",
      "Dani Hyland",
      "Flynn Haward",
      "Alanah Rogers",
      "Tam Robertson"
    ],
    "Company Number": "1894987",
    "Principal Place of Business": [
      "89 Tipton Parade",
      "",
      "Jerniganton",
      "6838"
    ],
    "NZBN": "9429127767329",
    "Email": "sales@interoakleyscape.co.nz",
    "Start Date": "27/12/2011",
    "Location": "Jerniganton"
  },
  {
    "Business Status": "Struck off",
    "Website": "http://www.butcher.com",
    "Registered Address": [
      "46/100c Bristol Avenue",
      "Strudwick",
      "Atterberry City",
      "5904"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Whitaker Solutions",
    "Business Industry Classification": "Information media and telecoms",
    "Address for Service": [
      "42 Padmore Road",
      "Rains",
      "Stantonland",
      "8542"
    ],
    "Phone Number": "+64 22 8593826",
    "Legal Entity Name": "Butcher Limited",
    "Directors": [
      "Rosalind Quigg",
      "Yasmin Clausson",
      "Shiree Arrington",
      "Charlotte Arterberry",
      "Lexa Brasher",
      "Troy Cropper",
      "Vanessa Barton",
      "Kirk Herbertson"
    ],
    "Company Number": "1895565",
    "Principal Place of Business": [
      "46/100c Bristol Avenue",
      "Strudwick",
      "Atterberry City",
      "5904"
    ],
    "NZBN": "9429127776291",
    "Email": "admin@butcher.com",
    "Start Date": "28/06/1991",
    "Location": "Atterberry City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.earlsonsolutions.co.nz",
    "Registered Address": [
      "67 Disney Street",
      "",
      "Jarvisville",
      "4784"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "82 Marston St",
      "Herbertson",
      "Lockwood",
      "5982"
    ],
    "Phone Number": "+64 29 8122707",
    "Legal Entity Name": "Earlson Solutions Limited",
    "Directors": [
      "Carey Leavitt",
      "Wade Aiken",
      "Michael Horne",
      "Donella Ellisson",
      "Ada Edwardson",
      "Jasmine Appleby",
      "Celestine Killam"
    ],
    "Company Number": "1896502",
    "Principal Place of Business": [
      "12 Simpson St",
      "School",
      "Pageton",
      "2120"
    ],
    "NZBN": "9429127783510",
    "Email": "charlie@earlsonsolutions.co.nz",
    "Start Date": "28/08/1987",
    "Location": "Pageton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.dannielnetworks.co.nz",
    "Registered Address": [
      "33/53 Barton Highway",
      "",
      "Harford",
      "2223"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Danniel Networks",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "33/53 Barton Highway",
      "",
      "Harford",
      "2223"
    ],
    "Phone Number": "+64 29 8537064",
    "Legal Entity Name": "Danniel Networks Limited",
    "Directors": [
      "Foster Teel",
      "Yves Timothyson",
      "Norman Hawking",
      "Cheryl Southgate"
    ],
    "Company Number": "1897237",
    "Principal Place of Business": [
      "33/53 Barton Highway",
      "",
      "Harford",
      "2223"
    ],
    "NZBN": "9429127792932",
    "Email": "sales@dannielnetworks.co.nz",
    "Start Date": "05/04/1999",
    "Location": "Harford"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.harfordsolutions.nz",
    "Registered Address": [
      "35 Wakefield Rd",
      "",
      "Whinery City",
      "1752"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "27 Paulson Road",
      "Atterberry",
      "Derricksonton",
      "8722"
    ],
    "Phone Number": "+64 29 5269401",
    "Legal Entity Name": "Harford Solutions Limited",
    "Directors": [
      "Ryan Olhouser"
    ],
    "Company Number": "1897540",
    "Principal Place of Business": [
      "35 Wakefield Rd",
      "",
      "Whinery City",
      "1752"
    ],
    "NZBN": "9429127796909",
    "Email": "sales@harfordsolutions.nz",
    "Start Date": "08/04/2003",
    "Location": "Whinery City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.maytechnologies.co.nz",
    "Registered Address": [
      "24/58 Hurst Rd",
      "Dwerryhouse",
      "Pickleton",
      "4207"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Accomodation",
    "Address for Service": [
      "24/30 Daniell St",
      "",
      "Elliotton",
      "3771"
    ],
    "Phone Number": "+64 23 7142158",
    "Legal Entity Name": "May Technologies Limited",
    "Directors": [
      "Yves Lum",
      "Stella Harden",
      "Annalisa Josephs",
      "Madeline Quick",
      "Scott Rains",
      "Daniel Franklin"
    ],
    "Company Number": "1897642",
    "Principal Place of Business": [
      "95i Thacker Rd",
      "",
      "Shakesheave City",
      "2889"
    ],
    "NZBN": "9429127798415",
    "Email": "office@maytechnologies.co.nz",
    "Start Date": "03/03/1993",
    "Location": "Shakesheave City"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.dawson.co.nz",
    "Registered Address": [
      "61 Osborne St",
      "Green",
      "Danielland",
      "3511"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Dawson",
    "Business Industry Classification": "Tourism",
    "Address for Service": [
      "49/19 Porter Road",
      "Sexton",
      "Barton City",
      "8342"
    ],
    "Phone Number": "+64 8 8868571",
    "Legal Entity Name": "Dawson Limited",
    "Directors": [
      "Tucker Morse",
      "Polly Gilliam",
      "Otto Readdie",
      "Tracey Kitchens",
      "Wyatt Bonham",
      "Robert Mathers",
      "Milton Sempers",
      "Wilfred Sandford"
    ],
    "Company Number": "1898323",
    "Principal Place of Business": [
      "61 Osborne St",
      "Green",
      "Danielland",
      "3511"
    ],
    "NZBN": "9429127803294",
    "Email": "alysen@dawson.co.nz",
    "Start Date": "17/12/2001",
    "Location": "Danielland"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.blackmancarterandchristophers.nz",
    "Registered Address": [
      "92 Johnson Road",
      "Causey",
      "Wyghtton",
      "7359"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Information communications and technology",
    "Address for Service": [
      "14l Bailey Avenue",
      "Hope",
      "Bristowton",
      "3994"
    ],
    "Phone Number": "+64 24 7037514",
    "Legal Entity Name": "Blackman, Carter and Christophers Limited",
    "Directors": [
      "Paul Forester",
      "Aletia James",
      "Kristal Clausson",
      "Sadie Booner",
      "William Babcock"
    ],
    "Company Number": "1898449",
    "Principal Place of Business": [
      "14l Bailey Avenue",
      "Hope",
      "Bristowton",
      "3994"
    ],
    "NZBN": "9429127804932",
    "Email": "office@blackmancarterandchristophers.nz",
    "Start Date": "07/03/2004",
    "Location": "Bristowton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ffe.co.nz",
    "Registered Address": [
      "96 Attwater Rd",
      "Rigby",
      "Williamton",
      "3133"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Hardwickcorp",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "400 Spalding Avenue",
      "Yates",
      "Arnold City",
      "4355"
    ],
    "Phone Number": "+64 17 6533048",
    "Legal Entity Name": "FFE Limited",
    "Directors": [
      "Wilfred Bonney",
      "Logan Anderson",
      "Rachelle Winter"
    ],
    "Company Number": "1898758",
    "Principal Place of Business": [
      "96 Attwater Rd",
      "Rigby",
      "Williamton",
      "3133"
    ],
    "NZBN": "9429127811220",
    "Email": "info@ffe.co.nz",
    "Start Date": "05/05/1991",
    "Location": "Williamton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ager.co.nz",
    "Registered Address": [
      "53 Nye Rd",
      "Peters",
      "Murgatroydton",
      "2061"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Heath",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "53 Nye Rd",
      "Peters",
      "Murgatroydton",
      "2061"
    ],
    "Phone Number": "+64 15 5537564",
    "Legal Entity Name": "AGER Limited",
    "Directors": [
      "Dylan Quincey",
      "Alicia Swindlehurst",
      "Rosemary Low",
      "Kathe Blackwood"
    ],
    "Company Number": "1899529",
    "Principal Place of Business": [
      "53 Nye Rd",
      "Peters",
      "Murgatroydton",
      "2061"
    ],
    "NZBN": "9429127814757",
    "Email": "sales@ager.co.nz",
    "Start Date": "22/10/2007",
    "Location": "Murgatroydton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.asc.nz",
    "Registered Address": [
      "75 Lucas Street",
      "Verity",
      "Scrivens City",
      "2101"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "43 Hollands Street",
      "",
      "Symonston",
      "4464"
    ],
    "Phone Number": "+64 9 8650876",
    "Legal Entity Name": "ASC Limited",
    "Directors": [
      "Lacy Butler",
      "Harold Morris",
      "Susannah Simon",
      "Victor Hudnall",
      "Benjamin Dennel",
      "Brendan Madison"
    ],
    "Company Number": "1899969",
    "Principal Place of Business": [
      "7/85 Chamberlain Rd",
      "Glendon",
      "Sampsontown",
      "4463"
    ],
    "NZBN": "9429127816645",
    "Email": "office@asc.nz",
    "Start Date": "01/07/2005",
    "Location": "Sampsontown"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.slatertech.co.nz",
    "Registered Address": [
      "43/68d Richardson Street",
      "",
      "Dexterton",
      "6223"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "Slatertech",
    "Business Industry Classification": "Agriculture, horticulture, and forestry",
    "Address for Service": [
      "43/68d Richardson Street",
      "",
      "Dexterton",
      "6223"
    ],
    "Phone Number": "+64 8 8703404",
    "Legal Entity Name": "Slatertech Limited",
    "Directors": [
      "Matthew Quincy"
    ],
    "Company Number": "1900210",
    "Principal Place of Business": [
      "43/68d Richardson Street",
      "",
      "Dexterton",
      "6223"
    ],
    "NZBN": "9429127822653",
    "Email": "admin@slatertech.co.nz",
    "Start Date": "17/04/2012",
    "Location": "Dexterton"
  },
  {
    "Business Status": "Active",
    "Website": "http://www.ckoo.com",
    "Registered Address": [
      "3 Loman Ave",
      "",
      "Jepsonville",
      "1561"
    ],
    "Entity Type": "New Zealand Company",
    "Trading name": "",
    "Business Industry Classification": "Retail trade",
    "Address for Service": [
      "46/13 William Parade",
      "Winston",
      "Simsland",
      "2012"
    ],
    "Phone Number": "+64 27 8310832",
    "Legal Entity Name": "CKOO Limited",
    "Directors": [
      "Derek Martins",
      "Daniel Alfson",
      "Parker Auttenberg",
      "Mindy Willis",
      "Belle Trent"
    ],
    "Company Number": "1900681",
    "Principal Place of Business": [
      "54 Hayward Rd",
      "Slater",
      "Fryton",
      "5733"
    ],
    "NZBN": "9429127831624",
    "Email": "admin@ckoo.com",
    "Start Date": "03/01/2006",
    "Location": "Fryton"
  }
]
