#---------------------------------------------------------------------------
# Utilities
#---------------------------------------------------------------------------

# Extend JQuery with functions for searching XML
# Based on http://www.ibm.com/developerworks/library/x-feedjquery/
# (nb: Internet Explorer 8 and earlier does not support localName)

getLocalName = (node) ->
    if node.localName?
        return node.localName
    else if node.prefix
        return node.nodeName[node.prefix.length+1..]
    else
        return node.nodeName

getAttribute = (node, localName, nsURI) ->
    for attr in node.attributes
        if getLocalName(attr) == localName and
           (nsURI == undefined or nsURI == node.namespaceURI)
            return attr.value
    return null

getFirst = (nodes, localName, nsURI) ->
    for node in nodes
        if getLocalName(node) == localName and
           (nsURI == undefined or nsURI == node.namespaceURI)
            return node
    return null

$.fn.extend
    no_ns_find: (localName) ->
        return $(@).find("*").filter ->
            return getLocalName(@) == localName

    ns_find: (nsURI, localName) ->
        return $(@).find("*").filter ->
            return @namespaceURI == nsURI and getLocalName(@) == localName

# RFC1422-compliant Javascript UUID function.
# From https://gist.github.com/bmc/1893440
uuid = ->
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) ->
    r = Math.random() * 16 | 0
    v = if c is 'x' then r else (r & 0x3|0x8)
    v.toString(16)
  )

#---------------------------------------------------------------------------
# NZBN Soap datasource
#---------------------------------------------------------------------------

class SoapDataSource
    constructor: ->
        console.log("NZBN_Glue.SoapDataSource constructor")
        @url = "/argonaut/nzbnmock/service/v1/search"
        @corens = "http://mbie.govt.nz/nzbn/core/1"          # ns2
        @srchns = "http://mbie.govt.nz/nzbn/search/1"        # ns3
        @subsns = "http://mbie.govt.nz/nzbn/subscription/1"  # ns4
        @xnlns  = "urn:oasis:names:tc:ciq:xnl:3"             # ns5
        @xalns  = "urn:oasis:names:tc:ciq:xal:3"             # ns8
        @xpilns = "urn:oasis:names:tc:ciq:xpil:3"            # ns9

    search: (term, filters, offset, limit) ->
        dfd = new $.Deferred()
        console.log("search for #{term}")

        if term.match(/94\d{11}/) and offset == 0 and limit > 0
            get(term)
            .done (result) -> dfd.resolve([result])
            .fail (result) -> console.log(result); dfd.resolve([])
            return dfd.promise()

        searchFilters =
            'start':           offset
            'limit':           limit
            'srch:searchTerm': term
        filter = filters['Business Status']
        if filter?
            ##console.log("  Business Status =? #{filter}")
            searchFilters['srch:businessStatus'] = filter
        filter = filters['Entity Type']
        if filter?
            ##console.log("  Entity Type =? #{filter}")
            searchFilters['srch:entityType'] = filter
        #searchFilters['srch:entityType'] = ["ASIC", "NON_ASIC"]
        filter = filters['Business Industry Classification']
        if filter?
            ##console.log("  Business Industry Classification =? #{filter}")
            searchFilters['srch:industryCode'] = filter
        
        $.soap
            url:          @url
            elementName:  "srch:searchEntityRequest"
            data:
                'core:messageContext':
                    'systemReference': uuid()
                    'correlationId':   "webui"
                    'timestamp':       @_now()
                    'context':         "search"
                'srch:filter': searchFilters
            envAttributes:
                'xmlns:core':  @corens
                'xmlns:srch':  @srchns
            #beforeSend: (envelope) ->
            #    confirm('ok search: '+envelope)
            success: (resp) =>
                ##console.log(resp.toString())
                results = @_parseSearchResults(resp.toXML())
                dfd.resolve(results)
            error: (resp) ->
                ##console.log(resp.toString())
                xmlDom = resp.toXML()
                dfd.reject($(xmlDom).no_ns_find("message").text())
        return dfd.promise()

    get: (nzbn) ->
        dfd = new $.Deferred()
        console.log("get #{nzbn}")

        $.soap
            url:         @url
            elementName: "srch:getEntityRequest"
            data:
                'core:messageContext':
                    'systemReference': uuid()
                    'correlationId':   "webui"
                    'timestamp':       @_now()
                    'context':         "get"
                'srch:nzbn': nzbn
            envAttributes:
                'xmlns:core': @corens
                'xmlns:srch': @srchns
            HTTPHeaders:
                'x-security-token': "123"
            #beforeSend: (envelope) ->
            #    confirm('ok get: '+envelope)
            success: (resp) =>
                ##console.log(resp.toString())
                xmlDom = resp.toXML()
                results = @_parseGetResult(resp.toXML())
                dfd.resolve(results)
            error: (resp) ->
                ##console.log(resp.toString())
                xmlDom = resp.toXML()
                dfd.reject($(xmlDom).no_ns_find("message").text())
        return dfd.promise()

    addWatch: (userID, nzbn) ->
        dfd = new $.Deferred()
        ##console.log("add watch on #{nzbn} TODO")
        dfd.resolve()
        return dfd.promise()

    removeWatch: (userID, nzbn) ->
        dfd = new $.Deferred()
        ##console.log("remove watch on #{nzbn} TODO")
        dfd.resolve()
        return dfd.promise()

    hasWatch: (userID, nzbn) ->
        dfd = new $.Deferred()
        ##console.log("add watch on #{nzbn} TODO")
        return dfd.promise()

    _now: ->
        d = new Date()
        return d.toISOString()

    _parseSearchResults: (xmlDom) ->
        searchResults = {}
        top = $(xmlDom).ns_find(@srchns, "searchResult")
        startText   = top.no_ns_find("start").text()
        countText   = top.no_ns_find("count").text()
        matchesText = top.no_ns_find("matches").text()
        searchResults['start']   = parseInt(startText)   or 0
        searchResults['count']   = parseInt(countText)   or 0
        searchResults['matches'] = parseInt(matchesText) or 0

        results = []
        top.ns_find(@srchns, "result").each (index, node) =>
            results.push(@_parseResultNode(node))
        searchResults['results'] = results
        return searchResults

    _parseGetResult: (xmlDom) ->
        result = {}
        entity = $(xmlDom).ns_find(@srchns, "entity")
        if entity
            result = @_parseResultNode(entity[0])
            result['NZBN'] = getAttribute(entity[0], "PartyID")

        return result

    _parseResultNode: (resultNode) ->
        result = {
          "NZBN":                             "",
          "Legal Entity Name":                "",
          "Business Industry Classification": "",
          "Start Date":                       "",
          "Business Status":                  "",
          "Entity Type":                      "",
          "Registered Address":               "",
          "Address for Service":              "",
          "Principal Place of Business":      "",
          "Directors":                        "",
          "Website":                          "",
          "Trading Name":                     "",
          "Previous Names":                   "",
          "Phone Number":                     "",
          "Email":                            "",
          "Company Number":                   "",
          "Last Updated":                     ""
        }
        resultNodes = resultNode.childNodes

        # Name
        orgNameNode = getFirst(resultNodes, "OrganisationName", @xnlns)
        $(orgNameNode).ns_find(@xnlns, "NameElement").each (index, node) =>
            if getAttribute(node, "ElementType") == "FullName"
                result['Legal Entity Name'] = $(node).text()

        # NZBN
        idsNodes = getFirst(resultNodes, "Identifiers", @xpilns)
        $(idsNodes).no_ns_find("Identifier").each (index, node) =>
            switch getAttribute(node, "Type")
                when "RegistrationID"
                    idElementNode = $(node).no_ns_find("IdentifierElement")
                    result['NZBN'] = idElementNode.text()
                when "CompanyID"
                    idElementNode = $(node).no_ns_find("IdentifierElement")
                    result['Company Number'] = idElementNode.text()

        # Addresses
        addressesNode = getFirst(resultNodes, "Addresses", @xpilns)
        $(addressesNode).no_ns_find("Address").each (index, node) =>
            address = @_parseAddressNode(node)
            if address
                addrType = @_parseAddressType(node)
                result[addrType] = address

        # Telephone
        contactNumsNode = getFirst(resultNodes, "ContactNumbers", @xpilns)
        $(contactNumsNode).no_ns_find("ContactNumber").each (index, node) =>
            if getAttribute(node, "CommunicationMediaType") == "Telephone"
                phone = @_parsePhoneNumber(node)
                if phone
                    result['Phone Number'] = phone
        
        # Email
        eAddrIdsNode = getFirst(resultNodes, "ElectronicAddressIdentifiers",
                                @xpilns)
        $(eAddrIdsNode).no_ns_find("ElectronicAddressIdentifier").each (index, node) =>
            text = $(node).text()
            if text
                switch getAttribute(node, "Type")
                    when "URL"
                        result['Website'] = text
                    when "EMAIL"
                        result['Email'] = text

        # Dates
        eventsNode = getFirst(resultNodes, "Events", @xpilns)
        $(eventsNode).no_ns_find("Event").each (index, node) =>
            date = getAttribute(node, "Date")
            switch getAttribute(node, "Type")
                when "Date of registration"
                    result['Start Date'] = date[..9]
                when "Last updated"
                    result['Last Updated'] = date[..9]

        # Directors
        directors = []
        relationshipsNode = getFirst(resultNodes, "Relationships", @xpilns)
        $(relationshipsNode).no_ns_find("Relationship").each (index, node) =>
            if getAttribute(node, "RelationshipWithPerson") == "Director"
                nameNode = getFirst(node.childNodes, "PersonName")
                name = @_parsePersonName(nameNode)
                if name
                    directors.push(name)
        if directors.length > 0
            result['Directors'] = directors

        # OrganisationInfo
        orgInfoNode = getFirst(resultNodes, "OrganisationInfo", @xpilns)
        orgType = getAttribute(orgInfoNode, "Type", @xpilns)
        switch orgType
            when 'LTD', 'UNLTD', 'COOP'
                result['Entity Type'] = "NZ Company (#{orgType})"
            when 'ASIC', 'NON_ASIC'
                result['Entity Type'] = "Overseas Company (#{orgType})"
        if getAttribute(orgInfoNode, "IndustryCodeType", @xpilns) == "BIC"
            bic = getAttribute(orgInfoNode, "IndustryCode", @xpilns)
            if bic?
                result['Business Industry Classification'] = bic
        orgStatus = getAttribute(orgInfoNode, "Status", @xpilns)
        if orgStatus?
            result['Business Status'] = orgStatus

        return result

    _parseAddressNode: (addressNode) ->
        address = []
        addr1 = addr2 = addr3 = addr4 = ""
        $(addressNode).ns_find(@xalns, "AddressLine").each (index, node) =>
            switch getAttribute(node, "Type")
                when "Address line 1"
                    addr1 = $(node).text()
                when "Address line 2"
                    addr2 = $(node).text()
                when "Address line 3"
                    addr3 = $(node).text()
                when "Address line 4"
                    addr4 = $(node).text()
        if addr1
            address.push(addr1)
        if addr2
            address.push(addr2)
        if addr3
            address.push(addr3)
        if addr4
            address.push(addr4)
        postcode = @_parsePostcode(addressNode)
        if postcode
            address.push(postcode)
        return address

    _parseAddressType: (addressNode) ->
        addrUsage = getAttribute(addressNode, "Usage")
        switch addrUsage
            when "Business"
                addrType = "Principal Place of Business"
            when "Billing"
                addrType = "Address for Service"
            when "Communication"
                addrType = "Address for Service"
            when "Contact"
                addrType = "Registered Address"
            else
                addrType = "#{addrUsage} Address"
        return addrType

    _parsePostcode: (addressNode) ->
        postcode = ""
        $(addressNode).ns_find(@xalns, "PostCode").children().each (index, node) =>
            if getAttribute(node, "Type") == "NZPostCode"
                postcode = $(node).text()
        return postcode

    _parsePhoneNumber: (phoneNode) ->
        area    = ""
        country = ""
        number  = ""
        $(phoneNode).no_ns_find("ContactNumberElement").each (index, node) =>
            text = $(node).text()
            switch getAttribute(node, "Type")
                when "AreaCode"
                    if text
                        area = text + " "
                when "CountryCode"
                    if text
                        country = text + " "
                when "LocalNumber"
                    if text
                        number = text
        # if country and area[0] == '0'
        #     area.splice(0, 1)
        return country + area + number

    _parsePersonName: (nameNode) ->
        name = []
        $(nameNode).ns_find(@xnlns, "NameElement").each (index, node) =>
            text = $(node).text()
            switch getAttribute(node, "ElementType")
                when "FirstName"
                    if text
                        name[0] = text + " "
                when "MiddleName"
                    if text
                        name[1] = text + " "
                when "LastName"
                    if text
                        name[2] = text
        return name.join('')

$ ->
    window.NZBN_Glue = new SoapDataSource()
    return

