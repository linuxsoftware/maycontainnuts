process.env.NODE_DISABLE_COLORS=1

gulp       = require "gulp"
util       = require "gulp-util"
plumber    = require "gulp-plumber"
notify     = require "gulp-notify"
notifier   = require "node-notifier"
beep       = require "beepbeep"
crashsound = require "gulp-crash-sound"
coffee     = require "gulp-coffee"
sourcemaps = require "gulp-sourcemaps"

gulp.task 'hello', ->
  beep(1)
  console.log "Hello World"
  gulp.src  "."
      .pipe notify "Hello World"

gulp.task "src.coffee", ->
  gulp.src  "src/*.coffee"
      .pipe plumber crashsound.plumb notify.onError (err) ->
          util.log(err)
          #beep()
          title:   "Burnt the coffee"
          message: "#{err}"
      .pipe sourcemaps.init()
      .pipe coffee({'no-header': true})
      .pipe sourcemaps.write("maps/")
      .pipe gulp.dest "static/js/"

gulp.task 'build', ['src.coffee']
gulp.task 'watch', ->
  gulp.watch("src/*.coffee", ['src.coffee'])

gulp.task('default', ['watch', 'build'])
