(function() {
  var ChunkSize, addResult, addShortResult, clearOutput, decodeHtmlEntity, get, loadMoreResults, onChangeSync, onHashChange, pushSearchState, search;

  clearOutput = function() {
    var searchOutputDiv, searchStatus;
    searchOutputDiv = $(".search-results .output");
    searchStatus = $("#search-status");
    searchOutputDiv.empty();
    searchStatus.text("");
  };

  addShortResult = function(result) {
    var fieldDiv, heading, i, key, len, link, name, ref, resultDiv, searchOutputDiv, uri, value;
    searchOutputDiv = $(".search-results .output");
    resultDiv = $('<div class="result" />');
    searchOutputDiv.append(resultDiv);
    heading = $('<h3 />');
    name = decodeHtmlEntity(result['Legal Entity Name']);
    if (result['NZBN'] != null) {
      uri = "#details=" + result['NZBN'];
      link = $("<a href=\"" + uri + "\">" + name + "</a>");
      heading.append(link);
    } else {
      heading.text(name);
    }
    resultDiv.append(heading);
    ref = ["NZBN", "Entity Type", "Company Number", "Trading name", "Business Industry Classification", "Business Status"];
    for (i = 0, len = ref.length; i < len; i++) {
      key = ref[i];
      value = result[key];
      if (!value) {
        value = "---";
      }
      fieldDiv = $('<div class="field" />');
      resultDiv.append(fieldDiv);
      fieldDiv.append($('<label>').text(key));
      fieldDiv.append($('<span>').text(value));
    }
  };

  addResult = function(result) {
    var fieldDiv, i, key, len, newValue, resultDiv, searchOutputDiv, val, value;
    searchOutputDiv = $(".search-results .output");
    resultDiv = $('<div class="result" />');
    searchOutputDiv.append(resultDiv);
    for (key in result) {
      value = result[key];
      if (!value) {
        value = "---";
      }
      fieldDiv = $('<div class="field" />');
      resultDiv.append(fieldDiv);
      fieldDiv.append($('<label>').text(key));
      if (Array.isArray(value)) {
        newValue = [];
        for (i = 0, len = value.length; i < len; i++) {
          val = value[i];
          newValue.push(decodeHtmlEntity(val));
        }
        value = newValue;
      } else {
        value = decodeHtmlEntity(value);
      }
      fieldDiv.append($('<span>').text(value));
    }
  };

  decodeHtmlEntity = function(str) {
    if (str && str.replace) {
      str = str.replace("&amp;", "&");
      str = str.replace("&lt;", "<");
      str = str.replace("&gt;", ">");
    }
    return str;
  };

  search = function(term, industry, biztypes, bizstatus2, offset) {
    var filters;
    if (offset == null) {
      offset = 0;
    }
    if (!term) {
      return;
    }
    filters = {};
    if (industry) {
      filters['Business Industry Classification'] = industry;
    }
    if (biztypes) {
      filters['Entity Type'] = biztypes;
    }
    if (bizstatus2) {
      filters['Business Status'] = bizstatus2;
    }
    NZBN_Glue.search(term, filters, 0, offset + ChunkSize).done(function(searchResults) {
      var i, img, len, ref, result, searchOutputDiv;
      if (offset === 0) {
        clearOutput();
      }
      ref = searchResults['results'].slice(offset, +(offset + ChunkSize) + 1 || 9e9);
      for (i = 0, len = ref.length; i < len; i++) {
        result = ref[i];
        addShortResult(result);
      }
      $("#loadMoreResults").remove();
      if (searchResults['count'] < searchResults['matches']) {
        searchOutputDiv = $(".search-results .output");
        img = $('<img id="loadMoreResults" src="/static/img/ajax-loader.gif"/>');
        return searchOutputDiv.append(img);
      }
    }).fail(function(msg) {
      return alert(msg);
    });
  };

  get = function(nzbn) {
    NZBN_Glue.get(nzbn).done(function(result) {
      clearOutput();
      return addResult(result);
    }).fail(function(msg) {
      return alert(msg);
    });
  };

  onChangeSync = function(elem, key) {
    var value;
    value = $.bbq.getState(key) || "";
    if (elem.val() !== value) {
      elem.val(value);
    }
    return value;
  };

  onHashChange = function(ev) {
    var activeStatusFilterInput, bizstatus1, bizstatus2, biztypes, i, inactiveStatusFilterInput, industry, len, nzbn, searchStatus, term, value;
    term = onChangeSync($("#searchEntityInput"), 'search');
    industry = onChangeSync($("#industryFilterInput"), 'industry');
    $("#typeFilterCheckboxes input:checkbox").prop("checked", false);
    biztypes = $.bbq.getState('biztype') || [];
    for (i = 0, len = biztypes.length; i < len; i++) {
      value = biztypes[i];
      $("#typeFilterCheckboxes input[value=" + value + "]").prop("checked", true);
    }
    bizstatus1 = onChangeSync($("#statusFilterInput"), 'bizstatus1');
    activeStatusFilterInput = $("#activeStatusFilterInput");
    inactiveStatusFilterInput = $("#inactiveStatusFilterInput");
    if (bizstatus1 === "ACTIVE") {
      activeStatusFilterInput.show();
      inactiveStatusFilterInput.hide();
      bizstatus2 = onChangeSync(activeStatusFilterInput, 'bizstatus2');
    } else if (bizstatus1 === "INACTIVE") {
      activeStatusFilterInput.hide();
      inactiveStatusFilterInput.show();
      bizstatus2 = onChangeSync(inactiveStatusFilterInput, 'bizstatus2');
    } else {
      activeStatusFilterInput.hide();
      inactiveStatusFilterInput.hide();
    }
    if (term || industry || biztypes || bizstatus2) {
      searchStatus = $("#search-status");
      if (term.length > 3) {
        searchStatus.text("Searching for " + term);
        search(term, industry, biztypes, bizstatus2);
      } else {
        clearOutput();
      }
    } else {
      nzbn = $.bbq.getState('details') || "";
      if (nzbn.length === 13) {
        get(nzbn);
      } else {
        clearOutput();
      }
    }
    return false;
  };

  loadMoreResults = function() {
    var bizstatus1, bizstatus2, biztypes, industry, term;
    term = $("#searchEntityInput").val();
    industry = $("#industryFilterInput").val();
    biztypes = $("#typeFilterCheckboxes input:checkbox:checked").map(function() {
      return this.value;
    }).get();
    bizstatus1 = $("#statusFilterInput").val();
    if (bizstatus1 === "ACTIVE") {
      bizstatus2 = $("#activeStatusFilterInput").val();
    } else if (bizstatus1 === "INACTIVE") {
      bizstatus2 = $("#inactiveStatusFilterInput").val();
    }
    loadMoreResults.offset += ChunkSize;
    search(term, industry, biztypes, bizstatus2, loadMoreResults.offset);
  };

  loadMoreResults.offset = 0;

  pushSearchState = function(key, value) {
    var state;
    state = $.bbq.getState();
    delete state['details'];
    state[key] = value;
    $.bbq.pushState(state, 2);
  };

  $(function() {
    var activeStatusFilterInput, inactiveStatusFilterInput, searchInput, searchStatus;
    $(window).on('hashchange', onHashChange);
    searchInput = $("#searchEntityInput");
    searchStatus = $("#search-status");
    searchInput.val("");
    searchInput.on("input propertychange paste", $.debounce(100, function(ev) {
      var term;
      term = $(this).val();
      if (term.length > 3) {
        pushSearchState('search', term);
      }
      return false;
    }));
    $("#industryFilterInput").change(function(ev) {
      pushSearchState('industry', $(this).val());
      return false;
    });
    $("#typeFilterCheckboxes input:checkbox").change(function(ev) {
      var biztypes;
      biztypes = $("#typeFilterCheckboxes input:checkbox:checked").map(function() {
        return this.value;
      }).get();
      pushSearchState('biztype', biztypes);
      return false;
    });
    activeStatusFilterInput = $("#activeStatusFilterInput");
    inactiveStatusFilterInput = $("#inactiveStatusFilterInput");
    $("#statusFilterInput").change(function(ev) {
      var bizstatus1;
      bizstatus1 = $(this).val();
      pushSearchState('bizstatus1', bizstatus1);
      if (bizstatus1 === "ACTIVE") {
        pushSearchState('bizstatus2', activeStatusFilterInput.val());
      } else if (bizstatus1 === "INACTIVE") {
        pushSearchState('bizstatus2', inactiveStatusFilterInput.val());
      }
      return false;
    });
    activeStatusFilterInput.change(function(ev) {
      pushSearchState('bizstatus2', $(this).val());
      return false;
    });
    inactiveStatusFilterInput.change(function(ev) {
      pushSearchState('bizstatus2', $(this).val());
      return false;
    });
    $(window).trigger('hashchange');
    $(window).scroll(function(ev) {
      if ($(window).scrollTop() === $(document).height() - $(window).height()) {
        return loadMoreResults();
      }
    });
  });

  ChunkSize = 20;

}).call(this);

//# sourceMappingURL=maps/may_contain_nuts.js.map